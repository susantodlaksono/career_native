$(function () {
 	'use restrict';

 	$(this).on('submit', container + ' #form-upload-cv', function (e){
        var form = $(this);
        $(this).ajaxSubmit({
           url: site_url + '/dashboard/widget/upload_cv',
           type: 'POST',
           data: {
            'csrf_token_app' : $('#csrf').val()
            },
           dataType: 'JSON',
            beforeSend: function(){
               form.find('[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i> Uploading...');
               form.find('[type="submit"]').attr('disabled', 'disabled');
            },
            success: function(r) {
               // if(r.sessionapp){
                  $('#csrf').val(r.csrf);
                 	form.find('[type="submit"]').html('Upload');
                 	form.find('[type="submit"]').removeAttr('disabled');
                 	if(r.success){
                 		form.find('input[name="file_before"]').val(r.file_name);
                 		$('.result-cv').html('<h4 class="text-center">Your CV :</h4><br> <a href="'+site_url+'/dashboard/widget/download/'+r.file_name+'">'+r.temp_name+'</a>');
                    	$('.container-fluid').pgNotification({
                       	style: 'flip',
                       	message: r.msg,
                       	position: 'top-right',
                       	// timeout: 1000,
                       	type: 'success'
                    	}).show();
                    	form.resetForm();
                  }else{
                     $('.container-fluid').pgNotification({
                        style: 'flip',
                        message: r.msg,
                        position: 'top-right',
                        // timeout: 1000,
                        type: 'danger'
                     }).show();
                  }
               // }else{
               //    window.location.href = location;
               // }
            },
           error: function() {
               form.find('[type="submit"]').html('Upload');
               form.find('[type="submit"]').removeAttr('disabled');
           }
        });
        e.preventDefault();
 	});

   $(this).on('submit', container + ' #form-upload-registration', function (e){
        var form = $(this);
        $(this).ajaxSubmit({
           url: site_url + '/dashboard/widget/upload_registration',
           type: 'POST',
           data: {
            'csrf_token_app' : $('#csrf').val()
            },
           dataType: 'JSON',
            beforeSend: function(){
               form.find('[type="submit"]').html('<i class="fa fa-spinner fa-spin"></i> Uploading...');
               form.find('[type="submit"]').attr('disabled', 'disabled');
            },
            success: function(r) {
               // if(r.sessionapp){
                  $('#csrf').val(r.csrf);
                  form.find('[type="submit"]').html('Upload');
                  form.find('[type="submit"]').removeAttr('disabled');
                  if(r.success){
                     form.find('input[name="file_before"]').val(r.file_name);
                     $('.result-registration').html('<h6 class="text-center bold" class="text-center bold" style="font-size:13px;">Your CV :</h6><h6 class="text-center font-arial" style="font-size:13px;"><a href="'+site_url+'/dashboard/widget/download_reg/'+r.file_name+'">'+r.temp_name+'</a></h6>');
                     $('.container-fluid').pgNotification({
                        style: 'flip',
                        message: r.msg,
                        position: 'top-right',
                        // timeout: 1000,
                        type: 'success'
                     }).show();
                     form.resetForm();
                  }else{
                     $('.container-fluid').pgNotification({
                        style: 'flip',
                        message: r.msg,
                        position: 'top-right',
                        // timeout: 1000,
                        type: 'danger'
                     }).show();
                  }
               // }else{
               //    window.location.href = location;
               // }
            },
           error: function() {
               form.find('[type="submit"]').html('Upload');
               form.find('[type="submit"]').removeAttr('disabled');
           }
        });
        e.preventDefault();
   });

 	$(this).on('click', container + ' .apply-test', function (e){
	   var id = $(this).data('id');
      var now = new Date();
      var year    = now.getUTCFullYear();
      var month   = now.getUTCMonth() + 1; 
      var day     = now.getUTCDate();
      var hour    = now.getUTCHours();
      var minute  = (now.getUTCMinutes()<10?'0':'') + now.getUTCMinutes();
      var second  = now.getUTCSeconds(); 
      var time_now_js = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;

      // var currentdate = new Date(); 
      // var time_now_js = currentdate.getFullYear() + "-"
      //           + (currentdate.getMonth()+1)  + "-" 
      //           + currentdate.getDate() + " "  
      //           + currentdate.getHours() + ":"  
      //           + currentdate.getMinutes() + ":" 
      //           + currentdate.getSeconds();

      ajaxManager.addReq({
         url: site_url + '/dashboard/widget/registration_test',
         type: 'POST',
         dataType: 'JSON',
         data: {
            id: id,
            time_now_js: time_now_js,
            now: now,
            'csrf_token_app' : $('#csrf').val()
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r) {
            $('#csrf').val(r.csrf);

            if(r.success) {
               Widget.Loader('test', {id : id}, 'container-content', false);
            }else{
               alert(r.msg);
            }
         }
      });
      e.preventDefault(); 		
 	});



});