$(function () {
	var container = '#widget-job-assignment-leader-' + uniqid;

	$(this).on('change', container + ' input[name="option_job"]', function(e) {
        $('.loading').show();
        $(this).get_description_job();
    });

    $.fn.get_description_job = function(params) {
        var p = $.extend({
            job_id : $(container + ' input[name="option_job"]:checked').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/job_assignment_leader/widget/get_description_job',
            type: 'POST',
            dataType: 'JSON',
            data: {
                job_id: p.job_id
            },
            beforeSend: function () {
                $('.loading').html('<h4>Loaded Description...</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
	         	error_handle(jqXHR, status, errorThrown);
	      	},
            success: function(r) {
            	var t = '';
                if(r.sessionapp){
                    if(r.success) {
                    	$('#jobdesc-member').html(r.jobdesc_member);
                    	$('#jobdesc-activity').html(r.jobdesc_activity);
                    	$('#jobdesc-issue').html(r.jobdesc_issue);
                    	$('#jobdesc-progress').html(r.jobdesc_progress);
                        if(r.jobdesc_status == null || r.jobdesc_status == 0){
                            $('#jobdesc-status').html('<span class="label label-default">None</span>');
                        }
                        if(r.jobdesc_status == 1){
                            $('#jobdesc-status').html('<span class="label label-info">In Progress</span>');
                        }
                        if(r.jobdesc_status == 2){
                            $('#jobdesc-status').html('<span class="label label-success">Done</span>');
                        }
                        if(r.jobdesc_status == 6){
                            $('#jobdesc-status').html('<span class="label label-warning">Open</span>');
                        }
                        if(r.jobdesc_start_date !== '' && r.jobdesc_end_date !== ''){
                            $('#jobdesc-date').html(r.jobdesc_start_date+' to '+r.jobdesc_end_date);
                        }
                        if(r.jobdesc_start_date !== '' && r.jobdesc_end_date === ''){
                            $('#jobdesc-date').html(r.jobdesc_start_date);
                        }else{
                            $('#jobdesc-date').html('-');
                        }
                    }
                }else{
                    window.location.href = location;
                }
        	},
            complete: function () {
                $(this).get_contributor_by_pool();
            }
    	});
    }

    $.fn.get_contributor_by_pool = function(params) {
        var p = $.extend({
            job_id : $(container + ' input[name="option_job"]:checked').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/job_assignment_leader/widget/get_contributor_by_pool',
            type: 'POST',
            dataType: 'JSON',
            data: {
                job_id: p.job_id
            },
            beforeSend: function () {
                $('.loading').html('<h4>Loaded Contributor...</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                var t = '';
                if(r.success) {
                    t += '<div class="table-responsive">';
                        t += '<table class="table no-margin">';
                            t += '<tbody>';
                                if(r.response.length > 0){
                                    $.each(r.response, function(k ,v) {
                                        t += '<tr>';
                                          t += '<td class="">'+v.pool_name+'</td>';
                                          t += '<td class="">'+v.total+'</td>';
                                        t += '</tr>';
                                    });
                                }else{
                                    t += '<tr>';
                                        t += '<td colspan="2"><h4 class="text-center text-danger">No Result</h4></td>';
                                    t += '</tr>';
                                }
                            t += '</tbody>';
                        t += '</table>';
                    t += '</div>';
                   
                }
                $('#section-member-by-pool').html(t);
            },
            complete: function () {
                $(this).get_main_task();
            }
        });
    }

    $.fn.get_main_task = function(params) {
        var p = $.extend({
            job_id : $(container + ' input[name="option_job"]:checked').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/job_assignment_leader/widget/get_main_task',
            type: 'POST',
            dataType: 'JSON',
            data: {
                job_id: p.job_id
            },
            beforeSend: function () {
                $('.loading').html('<h4>Loaded Main Task...</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                var t = '';
                if(r.success) {
                    if(r.response.length > 0){
                        $.each(r.response, function(k ,v) {
                            t += '<span class="label label-default" style="margin-right:5px;margin-bottom:5px;">'+v.main_task+'</span>';
                        });
                    }else{
                        t += '<h4 class="text-center text-danger">No Result</h4>';
                    }
                   
                }
                $('#section-main-task').html(t);
            },
            complete: function () {
                $(this).get_activity_job();
            }
        });
    }

    $.fn.get_activity_job = function(params) {
        var p = $.extend({
            offset: 0,
            currentPage : 1,
            limit : 5,
            load : false,
            job_id : $(container + ' input[name="option_job"]:checked').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/job_assignment_leader/widget/get_activity_job',
            type: 'POST',
            dataType: 'JSON',
            data: {
                job_id : p.job_id,
                offset : p.offset,
                limit : p.limit
            },
            beforeSend: function () {
                if(p.load){
                    $('.loading').show();    
                }
                $('.loading').html('<h4>Loaded Activity...</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                var t = '';
                if(r.sessionapp){
                    if(r.success) {
                        if(r.total > 0){
                            $.each(r.response, function(k ,v) {
                                t += '<div class="card share full-width">';
                                    t += '<div class="pull-right" style="margin-top:15px;margin-right:10px;">';
                                        t += status_name(v.status, '');
                                    t += '</div>';
                                    t += '<div class="card-header clearfix">';
                                        t += '<h5>'+v.member_name+'</h5>';
                                        t += '<h6>'+v.date+'</h6>';
                                    t += '</div>';
                                    t += '<div class="card-description">';
                                        t += '<p>'+v.description+'</p>';
                                        if(v.log_activity){
                                            t += '<hr>';
                                            $.each(v.log_activity, function(kk ,vv) {
                                                t += '<div class="" style="font-size: 12.5px;padding-bottom:3px;"><span class="text-black"><i class="fa fa-clock-o"></i> '+vv.date+'</span> '+status_name(vv.status_before)+' <i class="fa fa-arrow-circle-right"></i> '+status_name(vv.status_after)+'</div>';
                                            });
                                        }
                                    t += '</div>';
                                t += '</div>';
                            });
                        }else{
                            t += '<h4 class="text-center text-danger">No Result</h4>';
                        }
                    }
                    $('#container-activity').html(t);
                    $(this).paging_activity({
                        target: '.pagination-activity',
                        items : r.total,
                        currentPage : p.currentPage
                    });
                }else{
                    window.location.href = location;
                }
            },
            complete: function () {
                if(!p.load){
                    $(this).get_member_job();
                }else{
                    $('.loading').hide();    
                }
                
            }
        });
    }

    $.fn.get_member_job = function(params) {
        var p = $.extend({
            load : false,
            offset: 0,
            currentPage : 1,
            limit : 10,
            job_id : $(container + ' input[name="option_job"]:checked').val()
        }, params);
        ajaxManager.addReq({
            url: site_url + '/job_assignment_leader/widget/get_member_job',
            type: 'POST',
            dataType: 'JSON',
            data: {
                job_id: p.job_id,
                offset : p.offset,
                limit : p.limit
            },
            beforeSend: function () {
                if(p.load){
                    $('.loading').show();    
                }
                $('.loading').html('<h4>Loaded Member...</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                var t = '';
                var no = 0;
                if(r.sessionapp){
                    if(r.success) {
                        var t = '';
                        if(r.total > 0){
                            t += '<div class="table-responsive">';
                                t += '<table class="table table-hover table-condensed">';
                                    t += '<thead>';
                                        t += '<tr>';
                                            t += '<th style="width:5%">No</th>';
                                            t += '<th>Name</th>';
                                            t += '<th>Group</th>';
                                            t += '<th>Main Task</th>';
                                            t += '<th>Evaluation</th>';
                                            t += '<th></th>';
                                        t += '</tr>';
                                    t += '</thead>';
                                    t += '<tbody>';
                                        $.each(r.response, function(k ,v) {
                                            no++;
                                            t += '<tr>';
                                              t += '<td class="" style="width:5%">'+no+'</td>';
                                              t += '<td class="">'+v.member_name+'</td>';
                                              t += '<td class="">'+v.team_name+'</td>';
                                              t += '<td class="">'+(v.main_task ? v.main_task : 'None')+'</td>';
                                              t += '<td class="">'+(v.evaluation ? v.evaluation : 'None')+'</td>';
                                              t += '<td class=""><button type="button" class="btn btn-info btn-xs detail-member" data-id="'+v.id+'" data-name="'+v.member_name+'" data-evaluation="'+(v.evaluation ? v.evaluation : '')+'" data-group="'+v.team_name+'" data-group="'+v.team_name+'" data-join="'+moment(v.join_at).format('DD MMMM YYYY')+'">Detail</button></td>';
                                            t += '</tr>';
                                        });
                                    t += '</tbody>';
                                t += '</table>';
                            t += '</div>';
                        }else{
                            t += '<h4 class="text-center text-danger">No Result</h4>';
                        }
                        $('#container-member').html(t);
                       
                        $(this).paging_member({
                            target: '.pagination-member',
                            items : r.total,
                            currentPage : p.currentPage
                        });
                    }
                }else{
                    window.location.href = location;
                }
            },
            complete: function () {
                $('.loading').hide();
            }
        });
    }

    $(this).on('click', container + ' .detail-member', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var name = $(this).data('name');
        var group = $(this).data('group');
        var join = $(this).data('join');
        var evaluation = $(this).data('evaluation');
        $('#detail-member-name').html(name);
        $('#detail-member-desc').html('From '+group+' - Join project in '+join+'');
        $(this).get_detail_member({
            member_id : id,
            member_evaluation : evaluation
        });
    });

    $.fn.get_detail_member = function(params) {
        var p = $.extend({
            member_id : false,
            load : false,
            member_evaluation : ''
        }, params);
        ajaxManager.addReq({
            url: site_url + '/job_assignment_leader/widget/get_detail_member',
            type: 'POST',
            dataType: 'JSON',
            data: {
                member_id: p.member_id
            },
            beforeSend: function () {
                $('.loading').show();    
                $('.loading').html('<h4>Loaded Detail Member...</h4>');
            },
            error: function (jqXHR, status, errorThrown) {
                error_handle(jqXHR, status, errorThrown);
            },
            success: function(r) {
                var t = '';
                var no = 0;
                if(r.sessionapp){
                    if(r.success) {
                        var t = '';
                        // t += '<h4 class="bold text-complete">'+p.member_name+'</h4>';
                        // t += '<h6 class="bold">From '+p.member_group+' - Join project in '+p.member_join+'</h6>';
                        // t += '<hr>';
                        t += '<div class="row">';
                            t += '<div class="col-md-4">';
                                t += '<form id="form-new-main-task" method="post">';
                                    t += '<div class="form-group">';
                                        t += '<label>New Main Task</label>';
                                        t += '<input type="hidden" name="id" value="'+p.member_id+'">';
                                        t += '<input type="text" class="form-control" name="main_task" required>';
                                    t += '</div>';
                                    t += '<div class="form-group">';
                                        t += '<button type="submit" class="btn btn-info btn-xs pull-right">Create</button>';
                                    t += '</div>';
                                t += '</form>';
                                t += '<br><br>';
                                t += '<form id="form-edit-evaluation" method="post">';
                                    t += '<div class="form-group">';
                                        t += '<label>Evaluation</label>';
                                        t += '<input type="hidden" name="id" value="'+p.member_id+'">';
                                        t += '<input type="text" class="form-control" name="evaluation" value="'+p.member_evaluation+'" required>';
                                    t += '</div>';
                                    t += '<div class="form-group">';
                                        t += '<button type="submit" class="btn btn-info btn-xs pull-right">Change</button>';
                                    t += '</div>';
                                t += '</form>';
                            t += '</div>';
                            t += '<div class="col-md-8">';
                                if(r.response.length > 0){
                                    t += '<div class="table-responsive">';
                                        t += '<table class="table table-condensed" style="white-space:normal;">';
                                            t += '<thead>';
                                                t += '<tr>';
                                                    t += '<th>Main Task</th>';
                                                    t += '<th>Date</th>';
                                                t += '</tr>';
                                            t += '</thead>';
                                            t += '<tbody>';
                                                $.each(r.response, function(k ,v) {
                                                    no++;
                                                    t += '<tr>';
                                                      t += '<td class="">'+(v.main_task ? v.main_task : 'None')+'</td>';
                                                      t += '<td class="">'+moment(v.created_at).format('DD MMMM YYYY')+'</td>';
                                                    t += '</tr>';
                                                });
                                            t += '</tbody>';
                                        t += '</table>';
                                    t += '</div>';
                                }else{
                                    t += '<h4 class="text-center text-danger">No Result</h4>';
                                }
                            t += '</div>';
                        t += '</div>';
                        $('#container-detail-member').html(t);
                       
                        // $(this).paging_member({
                        //     target: '.pagination-member',
                        //     items : r.total,
                        //     currentPage : p.currentPage
                        // });
                    }
                }else{
                    window.location.href = location;
                }
            },
            complete: function () {
                $('.loading').hide();
                if(!p.load){
                    $('#modal-detail-member').modal('show');
                }
            }
        });
    }

    $(this).on('submit', container + ' #form-edit-evaluation', function (e) {
        e.preventDefault();
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/job_assignment_leader/widget/change_member_evaluation',
            type: 'POST',
            dataType: 'JSON',
            success: function(r) {
                if(r.success){
                    alert('Evaluation Changed');
                    $('#modal-detail-member').modal('hide');
                    $(this).get_member_job({ 
                        load : true
                    });
                }
            }
        });
    });

    $(this).on('submit', container + ' #form-new-main-task', function (e) {
        e.preventDefault();
        var form = $(this);
        $(this).ajaxSubmit({
            url: site_url + '/job_assignment_leader/widget/save_main_task_member',
            type: 'POST',
            dataType: 'JSON',
            success: function(r) {
                if(r.success){
                    alert('Main Task Created');
                    $(this).get_detail_member({ 
                        load : true,
                        member_id : r.job_member_id
                    });
                    $(this).get_member_job({ 
                        load : false
                    });
                }
            }
        });
    });

    $.fn.paging_activity = function(opt){
        var s = $.extend({
           items : 0,
           limit : 5,
           currentPage : 1
        }, opt);

        $(s.target).pagination({
            items: s.items,
            itemsOnPage: s.limit,
            prevText : '&laquo;',
            nextText : '&raquo;',
            hrefTextPrefix : '#',
            currentPage : s.currentPage,
            onPageClick : function(n,e){
                e.preventDefault();
                var offset = (n-1)*s.limit;
                $(this).get_activity_job({ 
                    offset : offset,
                    currentPage : n,
                    load : true
                });
            }
        });
    };

    $.fn.paging_member = function(opt){
        var s = $.extend({
           items : 0,
           limit : 10,
           currentPage : 1
        }, opt);

        $(s.target).pagination({
            items: s.items,
            itemsOnPage: s.limit,
            prevText : '&laquo;',
            nextText : '&raquo;',
            hrefTextPrefix : '#',
            currentPage : s.currentPage,
            onPageClick : function(n,e){
                e.preventDefault();
                var offset = (n-1)*s.limit;
                $(this).get_member_job({ 
                    offset : offset,
                    currentPage : n,
                    load : true
                });
            }
        });
    };


    $(this).get_description_job();
});	