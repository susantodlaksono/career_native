-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 30, 2021 at 05:30 AM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.3.33-1+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `career_yogi`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `id` int NOT NULL,
  `user_id` int UNSIGNED DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `age` tinyint DEFAULT NULL,
  `vacancy_division_id` int NOT NULL,
  `gender` enum('L','P') DEFAULT NULL,
  `education_degree` varchar(100) DEFAULT NULL,
  `school_majors` varchar(100) DEFAULT NULL,
  `university` varchar(100) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sc_twitter` varchar(250) DEFAULT NULL,
  `sc_facebook` varchar(250) DEFAULT NULL,
  `sc_instagram` varchar(250) DEFAULT NULL,
  `sc_portofolio` varchar(250) DEFAULT NULL,
  `sc_portofolio_file` text,
  `sc_linkedin` varchar(250) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` int DEFAULT NULL,
  `level` tinyint DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`id`, `user_id`, `name`, `birth_date`, `age`, `vacancy_division_id`, `gender`, `education_degree`, `school_majors`, `university`, `contact_number`, `email`, `sc_twitter`, `sc_facebook`, `sc_instagram`, `sc_portofolio`, `sc_portofolio_file`, `sc_linkedin`, `created_date`, `status`, `level`, `notes`) VALUES
(5, 7, 'Susanto', '1988-12-09', 33, 1, 'L', 'SLTA/Sederajat', 'TI', 'Widyatama', '0869898922', 'susantodlaksono@gmail.com', '', '', '', '', NULL, '', '2021-12-29 16:07:02', 2, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicant_file`
--

CREATE TABLE `applicant_file` (
  `id` int NOT NULL,
  `applicant_id` int DEFAULT NULL,
  `applicant_file` text,
  `applicant_temp_file` text,
  `applicant_type_file` tinyint NOT NULL COMMENT '1 : CV | 2 : Pas Photo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_file`
--

INSERT INTO `applicant_file` (`id`, `applicant_id`, `applicant_file`, `applicant_temp_file`, `applicant_type_file`) VALUES
(9, 5, '04070100000061cc2535d97d4.pdf', 'Real-Life BPMN - edition 4.pdf', 1),
(10, 5, '04070100000061cc2535dbcb6.png', 'Screenshot from 2021-10-07 11-04-56 (2).png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `evaluator_tests`
--

CREATE TABLE `evaluator_tests` (
  `id` int NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `test_type_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `date`) VALUES
(1, 'Admin', 'Administrator', '2016-05-22 21:48:44'),
(2, 'User', 'User/member', '2016-09-16 22:15:46'),
(3, 'Evaluator', 'Penilai test teknis', '2021-11-10 19:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int NOT NULL,
  `question_text` text,
  `correct_answers_id` int DEFAULT NULL,
  `correct_answers_multiple` text,
  `question_description` text,
  `test_type_id` int NOT NULL,
  `question_type_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question_text`, `correct_answers_id`, `correct_answers_multiple`, `question_description`, `test_type_id`, `question_type_id`) VALUES
(1, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(2, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(3, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(4, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(5, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(6, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(7, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(8, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(9, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(10, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(11, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(12, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(13, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(14, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(15, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(16, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(17, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(18, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(19, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(20, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(21, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(22, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(23, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(24, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(25, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(26, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(27, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(28, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(29, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(30, 'Pilih salah satu sesuai kepribadian anda', NULL, NULL, NULL, 1, 5),
(31, 'Suatu langkah untuk menyelesaikan sebuah masalah secara logis dan sistematis adalah...', NULL, NULL, NULL, 2, 1),
(32, 'Pseudocode yang dipakai dalam penulisan algoritma adalah...', NULL, NULL, NULL, 2, 1),
(33, 'Sintak yang tepat dan benar untuk mengeluarkan output \"Hello people\" di pemrograman C++ adalah...', NULL, NULL, NULL, 2, 1),
(34, 'Alat bantu untuk mengorganisasikan hasil analis adalah dengan...', NULL, NULL, NULL, 2, 1),
(35, 'Tipe data apakah yang bisa digunakan jika int x = 9.90', NULL, NULL, NULL, 2, 1),
(36, 'Setiap perintah dalam projek C++ selalu diakhiri dengan...', NULL, NULL, NULL, 2, 1),
(37, 'Suatu program yang memiliki statement serta instruksi yang bermaksud untuk tujuan tertentu disebut...', NULL, NULL, NULL, 2, 1),
(38, 'Bagian dari program yang memiliki peran sangat penting yang akan mempengaruhi setiap instruksi jalannya program adalah...', NULL, NULL, NULL, 2, 1),
(39, 'Untuk menyatakan fungsi utama berakhir di running dan dijalankan tanpa adanya debug atau eror adalah...', NULL, NULL, NULL, 2, 1),
(40, 'Cin dalam program C++ berati...', NULL, NULL, NULL, 2, 1),
(41, '4+4', NULL, NULL, NULL, 3, 1),
(42, '1+1', NULL, NULL, NULL, 3, 1),
(43, '8+8', NULL, NULL, NULL, 3, 1),
(44, '1 + 1', NULL, NULL, NULL, 3, 4),
(45, '2 + 2', NULL, NULL, NULL, 3, 4),
(51, 'Upload gambar hasil dari program kelipatan 2', NULL, NULL, NULL, 4, 2),
(52, 'Upload gambar hasil dari program pembagian  4', NULL, NULL, NULL, 4, 2),
(53, 'Upload gambar hasil dari program pertambahan  6', NULL, NULL, NULL, 4, 2),
(54, 'Upload gambar hasil dari program pengurangan 10', NULL, NULL, NULL, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

CREATE TABLE `question_answers` (
  `id` int NOT NULL,
  `question_id` int DEFAULT NULL,
  `question_answers` varchar(250) NOT NULL,
  `question_answers_alias` char(5) DEFAULT NULL,
  `question_type` enum('text','image') NOT NULL DEFAULT 'text',
  `correct_answers` tinyint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_answers`
--

INSERT INTO `question_answers` (`id`, `question_id`, `question_answers`, `question_answers_alias`, `question_type`, `correct_answers`) VALUES
(1, 1, 'Spontan, Fleksibel, tidak diikat waktu', 'P', 'text', NULL),
(2, 1, 'Terencana dan memiliki deadline jelas', 'J', 'text', NULL),
(3, 2, 'Lebih memilih berkomunikasi dengan menulis', 'I', 'text', NULL),
(4, 2, 'Lebih memilih berkomunikasi dengan bicara', 'E', 'text', NULL),
(5, 3, 'Tidak menyukai hal-hal yang bersifat mendadak dan di luar perencanaan', 'J', 'text', NULL),
(6, 3, 'Perubahan mendadak tidak jadi masalah', 'P', 'text', NULL),
(7, 4, 'Obyektif', 'T', 'text', NULL),
(8, 4, 'Subyektif', 'F', 'text', NULL),
(9, 5, 'Menemukan dan mengembangkan ide dengan mendiskusikannya', 'E', 'text', NULL),
(10, 5, 'Menemukan dan mengembangkan ide dengan merenungkan', 'I', 'text', NULL),
(11, 6, 'Bergerak dari gambaran umum baru ke detail', 'N', 'text', NULL),
(12, 6, 'Bergerak dari detail ke gambaran umum sebagai kesimpulan akhir', 'S', 'text', NULL),
(13, 7, 'Berorientasi pada dunia eksternal (kegiatan, orang)', 'E', 'text', NULL),
(14, 7, 'Berorientasi pada dunia internal (memori, pemikiran, ide)', 'I', 'text', NULL),
(15, 8, 'Berbicara mengenai masalah yang dihadapi hari ini dan langkah-langkah praktis mengatasinya', 'S', 'text', NULL),
(16, 8, 'Berbicara mengenai visi masa depan dan konsep-konsep mengenai visi tersebut', 'N', 'text', NULL),
(17, 9, 'Diyakinkan dengan penjelasan yang menyentuh perasaan', 'F', 'text', NULL),
(18, 9, 'Diyakinkan dengan penjelasan yang masuk akal', 'T', 'text', NULL),
(19, 10, 'Fokus pada sedikit hobi namun mendalam', 'I', 'text', NULL),
(20, 10, 'Fokus pada banyak hobi secara luas dan umum', 'E', 'text', NULL),
(21, 11, 'Tertutup dan mandiri', 'I', 'text', NULL),
(22, 11, 'Sosial dan ekspresif', 'E', 'text', NULL),
(23, 12, 'Aturan, jadwal dan target sangat mengikat dan membebani', 'P', 'text', NULL),
(24, 12, 'Aturan, jadwal dan target akan sangat membantu dan memperjelas tindakan', 'J', 'text', NULL),
(25, 13, 'Menggunakan pengalaman sebagai pedoman', 'S', 'text', NULL),
(26, 13, 'Menggunakan imajinasi dan perenungan sebagai pedoman', 'N', 'text', NULL),
(27, 14, 'Berorientasi tugas dan job description', 'T', 'text', NULL),
(28, 14, 'Berorientasi pada manusia dan hubungan', 'F', 'text', NULL),
(29, 15, 'Pertemuan dengan orang lain dan aktivitas sosial melelahkan', 'I', 'text', NULL),
(30, 15, 'Bertemu orang dan aktivitas sosial membuat bersemangat', 'E', 'text', NULL),
(31, 16, 'SOP sangat membantu', 'S', 'text', NULL),
(32, 16, 'SOP sangat membosankan', 'N', 'text', NULL),
(33, 17, 'Mengambil keputusan berdasar logika dan aturan main', 'T', 'text', NULL),
(34, 17, 'Mengambil keputusan berdasar perasaan pribadi dan kondisi orang lain', 'F', 'text', NULL),
(35, 18, 'Bebas dan dinamis', 'N', 'text', NULL),
(36, 18, 'Prosedural dan tradisional', 'S', 'text', NULL),
(37, 19, 'Berorientasi pada hasil', 'J', 'text', NULL),
(38, 19, 'Berorientasi pada proses', 'P', 'text', NULL),
(39, 20, 'Beraktifitas sendirian di rumah menyenangkan', 'I', 'text', NULL),
(40, 20, 'Beraktifitas sendirian di rumah membosankan', 'E', 'text', NULL),
(41, 21, 'Membiarkan orang lain bertindak bebas asalkan tujuan tercapai', 'P', 'text', NULL),
(42, 21, 'Mengatur orang lain dengan tata tertib agar tujuan tercapai', 'J', 'text', NULL),
(43, 22, 'Memilih ide inspiratif lebih penting daripada fakta', 'N', 'text', NULL),
(44, 22, 'Memilih fakta lebih penting daripada ide inspiratif', 'S', 'text', NULL),
(45, 23, 'Mengemukakan tujuan dan sasaran lebih dahulu', 'T', 'text', NULL),
(46, 23, 'Mengemukakan kesepakatan terlebih dahulu', 'F', 'text', NULL),
(47, 24, 'Fokus pada target dan mengabaikan hal-hal baru', 'J', 'text', NULL),
(48, 24, 'Memperhatikan hal-hal baru dan siap menyesuaikan diri serta mengubah target', 'P', 'text', NULL),
(49, 25, 'Kontinuitas dan stabilitas lebih diutamakan', 'S', 'text', NULL),
(50, 25, 'Perubahan dan variasi lebih diutamakan', 'N', 'text', NULL),
(51, 26, 'Pendirian masih bisa berubah tergantung situasi nantinya', 'P', 'text', NULL),
(52, 26, 'Berpegang teguh pada pendirian', 'J', 'text', NULL),
(53, 27, 'Bertindak step by step dengan timeframe yang jelas', 'S', 'text', NULL),
(54, 27, 'Bertindak dengan semangat tanpa menggunakan timeframe', 'N', 'text', NULL),
(55, 28, 'Berinisiatif tinggi hampir dalam berbagai hal meskipun tidak berhubungan dengan dirinya', 'E', 'text', NULL),
(56, 28, 'Berinisiatif bila situasi memaksa atau berhubungan dengan kepentingan sendiri', 'I', 'text', NULL),
(57, 29, 'Lebih memilih tempat yang tenang dan pribadi untuk berkonsentrasi', 'I', 'text', NULL),
(58, 29, 'Lebih memilih tempat yang ramai dan banyak interaksi / aktifitas', 'E', 'text', NULL),
(59, 30, 'Menganalisa', 'T', 'text', NULL),
(60, 30, 'Berempati', 'F', 'text', NULL),
(61, 31, 'Pseudocode', NULL, 'text', NULL),
(62, 31, 'Fungsi', NULL, 'text', NULL),
(63, 31, 'Array', NULL, 'text', NULL),
(64, 31, 'Algoritma', NULL, 'text', 1),
(65, 31, 'Pemrograman', NULL, 'text', NULL),
(66, 32, 'Bahasa pemrograman', NULL, 'text', 1),
(67, 32, 'Bahasa latin', NULL, 'text', NULL),
(68, 32, 'Bahasa Indonesia', NULL, 'text', NULL),
(69, 32, 'Bahasa terstruktur', NULL, 'text', NULL),
(70, 32, 'Bahasa sehari-hari', NULL, 'text', NULL),
(71, 33, 'Cin>>\"Hello people\";', NULL, 'text', NULL),
(72, 33, 'Cin>>\"Hello people;', NULL, 'text', NULL),
(73, 33, 'Cout<<\"Hello people', NULL, 'text', NULL),
(74, 33, 'Cout<<\"Hello people\";', NULL, 'text', 1),
(75, 33, 'Create table Hello people', NULL, 'text', NULL),
(76, 34, 'Input - Proses - Output', NULL, 'text', 1),
(77, 34, 'Start - Proses - Input', NULL, 'text', NULL),
(78, 34, 'Start - Proses - Output', NULL, 'text', NULL),
(79, 34, 'Start - Input - End', NULL, 'text', NULL),
(80, 34, 'Output - Input - Proses', NULL, 'text', NULL),
(81, 35, 'Array', NULL, 'text', NULL),
(82, 35, 'Float', NULL, 'text', 1),
(83, 35, 'Char', NULL, 'text', NULL),
(84, 35, 'Fungsi', NULL, 'text', NULL),
(85, 35, 'Dimensi dua', NULL, 'text', NULL),
(86, 36, '?>', NULL, 'text', NULL),
(87, 36, '//', NULL, 'text', NULL),
(88, 36, ';', NULL, 'text', 1),
(89, 36, '#', NULL, 'text', NULL),
(90, 36, '{}', NULL, 'text', NULL),
(91, 37, 'Aray', NULL, 'text', NULL),
(92, 37, 'Looping', NULL, 'text', NULL),
(93, 37, 'Fungsi', NULL, 'text', 1),
(94, 37, 'Variabel', NULL, 'text', NULL),
(95, 37, 'Tipe data', NULL, 'text', NULL),
(96, 38, 'Aray', NULL, 'text', NULL),
(97, 38, 'Looping', NULL, 'text', NULL),
(98, 38, 'Fungsi', NULL, 'text', NULL),
(99, 38, 'Variabel', NULL, 'text', NULL),
(100, 38, 'Tipe data', NULL, 'text', 1),
(101, 39, 'getch', NULL, 'text', NULL),
(102, 39, ';', NULL, 'text', NULL),
(103, 39, 'return0', NULL, 'text', 1),
(104, 39, 'using namespace std', NULL, 'text', NULL),
(105, 39, 'int main', NULL, 'text', NULL),
(106, 40, 'Untuk menhasilkan output', NULL, 'text', NULL),
(107, 40, 'Untuk menginput data', NULL, 'text', 1),
(108, 40, 'Untuk membuat program', NULL, 'text', NULL),
(109, 40, 'Untuk mengetahui adanya debug', NULL, 'text', NULL),
(110, 40, 'Untuk menampilkan output', NULL, 'text', NULL),
(111, 41, '8', NULL, 'text', 1),
(112, 41, '7', NULL, 'text', NULL),
(113, 41, '6', NULL, 'text', NULL),
(114, 41, '5', NULL, 'text', NULL),
(115, 41, '4', NULL, 'text', NULL),
(116, 42, '1', NULL, 'text', NULL),
(117, 42, '2', NULL, 'text', 1),
(118, 42, '4', NULL, 'text', NULL),
(119, 42, '8', NULL, 'text', NULL),
(120, 42, '10', NULL, 'text', NULL),
(121, 43, '10', NULL, 'text', NULL),
(122, 43, '12', NULL, 'text', NULL),
(123, 43, '17', NULL, 'text', NULL),
(124, 43, '16', NULL, 'text', 1),
(125, 43, '20', NULL, 'text', NULL),
(126, 44, '2', NULL, 'text', 1),
(127, 44, '2', NULL, 'text', 1),
(128, 44, '4', NULL, 'text', NULL),
(129, 44, '7', NULL, 'text', NULL),
(130, 44, '10', NULL, 'text', NULL),
(131, 45, '4', NULL, 'text', 1),
(132, 45, '2', NULL, 'text', NULL),
(133, 45, '4', NULL, 'text', 1),
(134, 45, '7', NULL, 'text', NULL),
(135, 45, '8', NULL, 'text', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question_file`
--

CREATE TABLE `question_file` (
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `temp_name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_image`
--

CREATE TABLE `question_image` (
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `temp_name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_type`
--

CREATE TABLE `question_type` (
  `id` int NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_type`
--

INSERT INTO `question_type` (`id`, `name`, `type`, `status`) VALUES
(1, 'Pilihan Ganda', 'Basic', 1),
(2, 'Upload File', 'Basic', 1),
(3, 'Jawaban Deskripsi', 'Basic', 1),
(4, 'Jawaban Ganda', 'Basic', 1),
(5, 'Psikotest', 'Special', 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_answers`
--

CREATE TABLE `test_answers` (
  `id` int NOT NULL,
  `question_id` int NOT NULL,
  `test_transaction_id` int NOT NULL,
  `answers` text,
  `answers_temp_file` text,
  `result` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_answers`
--

INSERT INTO `test_answers` (`id`, `question_id`, `test_transaction_id`, `answers`, `answers_temp_file`, `result`) VALUES
(2, 1, 29, 'P', NULL, NULL),
(3, 2, 29, 'E', NULL, NULL),
(4, 3, 29, 'J', NULL, NULL),
(5, 4, 29, 'F', NULL, NULL),
(6, 5, 29, 'E', NULL, NULL),
(7, 6, 29, 'S', NULL, NULL),
(8, 7, 29, 'E', NULL, NULL),
(9, 8, 29, 'N', NULL, NULL),
(10, 9, 29, 'F', NULL, NULL),
(11, 10, 29, 'E', NULL, NULL),
(12, 11, 29, 'I', NULL, NULL),
(13, 12, 29, 'P', NULL, NULL),
(14, 13, 29, 'N', NULL, NULL),
(15, 14, 29, 'T', NULL, NULL),
(16, 15, 29, 'E', NULL, NULL),
(17, 16, 29, 'S', NULL, NULL),
(18, 17, 29, 'T', NULL, NULL),
(19, 18, 29, 'S', NULL, NULL),
(20, 19, 29, 'J', NULL, NULL),
(21, 20, 29, 'E', NULL, NULL),
(22, 21, 29, 'P', NULL, NULL),
(23, 22, 29, 'N', NULL, NULL),
(24, 23, 29, 'F', NULL, NULL),
(25, 24, 29, 'P', NULL, NULL),
(26, 25, 29, 'S', NULL, NULL),
(27, 26, 29, 'P', NULL, NULL),
(28, 27, 29, 'S', NULL, NULL),
(29, 28, 29, 'E', NULL, NULL),
(30, 29, 29, 'I', NULL, NULL),
(31, 30, 29, 'T', NULL, NULL),
(32, 41, 30, '111', NULL, 1),
(33, 42, 30, '117', NULL, 1),
(34, 43, 30, '121', NULL, NULL),
(35, 44, 30, '[\"126\",\"127\"]', NULL, 1),
(36, 45, 30, '[\"131\",\"133\"]', NULL, 1),
(37, 31, 31, '61', NULL, NULL),
(38, 32, 31, '66', NULL, 1),
(39, 33, 31, '74', NULL, 1),
(40, 34, 31, '76', NULL, 1),
(41, 36, 31, '88', NULL, 1),
(42, 35, 31, '85', NULL, NULL),
(43, 37, 31, '93', NULL, 1),
(44, 38, 31, '99', NULL, NULL),
(45, 39, 31, '104', NULL, NULL),
(46, 40, 31, '108', NULL, NULL),
(47, 51, 32, '06192055161cc44381e3ef.png', 'Screenshot from 2021-10-07 11-04-56 (2).png', 8),
(48, 52, 32, '06192455261cc443c9a979.jpg', 'photo_2021-10-07_11-59-34 (2).jpg', 6),
(49, 53, 32, '06193155361cc44436a23d.png', 'Screenshot from 2021-10-07 11-04-56.png', 10),
(50, 54, 32, '06193555461cc4447e4ef0.png', 'Screenshot from 2021-10-07 11-04-56.png', 5);

-- --------------------------------------------------------

--
-- Table structure for table `test_assessment`
--

CREATE TABLE `test_assessment` (
  `id` int NOT NULL,
  `test_transaction_id` int NOT NULL,
  `question_type_id` int NOT NULL,
  `assessment` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_assessment`
--

INSERT INTO `test_assessment` (`id`, `test_transaction_id`, `question_type_id`, `assessment`) VALUES
(1, 29, 5, 'ESFP'),
(2, 30, 1, '66.67'),
(3, 30, 4, '100'),
(4, 31, 1, '50'),
(5, 32, 2, '72.5');

-- --------------------------------------------------------

--
-- Table structure for table `test_groups`
--

CREATE TABLE `test_groups` (
  `id` int NOT NULL,
  `vacancy_division_id` int NOT NULL,
  `test_type_id` int NOT NULL,
  `status` tinyint NOT NULL,
  `rules` varchar(30) NOT NULL DEFAULT 'opened',
  `random_test` tinyint DEFAULT NULL,
  `sort` tinyint NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_groups`
--

INSERT INTO `test_groups` (`id`, `vacancy_division_id`, `test_type_id`, `status`, `rules`, `random_test`, `sort`) VALUES
(1, 1, 1, 1, 'opened', NULL, 1),
(2, 1, 2, 1, '3', NULL, 3),
(3, 1, 3, 1, '1', NULL, 2),
(4, 1, 4, 1, '2', NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `test_transaction`
--

CREATE TABLE `test_transaction` (
  `id` int NOT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `applicant_id` int DEFAULT NULL,
  `test_type_id` int NOT NULL,
  `status` tinyint DEFAULT '0' COMMENT '0 : Register | 1 : Start | 2 : Finish',
  `checked_transaction` int DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_transaction`
--

INSERT INTO `test_transaction` (`id`, `time_start`, `time_end`, `applicant_id`, `test_type_id`, `status`, `checked_transaction`) VALUES
(29, '2021-12-29 17:28:05', '2021-12-29 17:58:05', 5, 1, 2, 0),
(30, '2021-12-29 18:17:35', '2021-12-29 18:27:35', 5, 3, 2, 0),
(31, '2021-12-29 18:18:03', '2021-12-29 18:48:03', 5, 2, 2, 0),
(32, '2021-12-29 18:19:15', '2021-12-29 18:39:15', 5, 4, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `test_type`
--

CREATE TABLE `test_type` (
  `id` int NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `mode_test` tinyint NOT NULL,
  `total_question` int NOT NULL,
  `time` int DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `sort` tinyint DEFAULT NULL,
  `type` enum('Fix','Custom') NOT NULL DEFAULT 'Custom',
  `noedited` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_type`
--

INSERT INTO `test_type` (`id`, `name`, `description`, `mode_test`, `total_question`, `time`, `status`, `sort`, `type`, `noedited`) VALUES
(1, 'PSIKOTEST MBTI', '<p>Di bawah ini ada 60 nomor. Masing-masing nomor memiliki dua pernyataan yang bertolak belakang (PERNYATAAN A &amp; B). Pilihlah salah satu pernyataan yang paling sesuai dengan diri Anda dengan mengklik radio button yang sudah disediakan. Anda HARUS memilih salah satu yang dominan serta mengisi semua nomor.</p>', 1, 30, 20, 1, NULL, 'Custom', 1),
(2, 'BASIC PEMOGRAMAN', NULL, 1, 40, 30, 1, NULL, 'Custom', NULL),
(3, 'MATEMATIKA DASAR', '<p>KERJAKAN SOAL YANG MUDAH TERLEBIH DAHULU</p>', 2, 10, 10, 1, NULL, 'Custom', NULL),
(4, 'SKILL PEMECAHAN MASALAH', '<p>KERJAKAN 5 SOAL BERIKUT</p>', 1, 5, 20, 1, NULL, 'Custom', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `password_show` varchar(225) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int UNSIGNED NOT NULL,
  `last_login` int UNSIGNED DEFAULT NULL,
  `active` tinyint UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `password_show`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`) VALUES
(1, '::1', 'admin', '$2a$08$d6dNzNvDgCVaKZQatI0HD.KJQJHJ0q/SvYiqnm8.7uatu0ikrf1Bu', NULL, NULL, 'administrator@bdg.ebdesk.com', NULL, NULL, NULL, '0M4FxzcUyRGd1jFJYL44Ju', 1460088952, 1640782188, 1, 'Administrator', '', NULL, '-', NULL),
(2, '::1', 'evaluator', '$2a$08$d6dNzNvDgCVaKZQatI0HD.KJQJHJ0q/SvYiqnm8.7uatu0ikrf1Bu', NULL, NULL, 'evaluator@bdg.ebdesk.com', NULL, NULL, NULL, '.PVIFJG/7kCfnhUTDCLsNe', 1460088952, 1630919777, 1, 'Evaluator', '', NULL, '-', NULL),
(7, '::1', 'susantodlaksono@gmail.com', '$2y$08$XasyLqhz3YCQZLiTXiY32eL9U3FCdSveU.tEtTFHgzci3lVGGEwQa', '086989892261cc2535dbe8a', NULL, 'susantodlaksono@gmail.com', NULL, NULL, NULL, 'Lebu..g8cmYZmgIsyHWR9e', 1640768821, 1640772619, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int UNSIGNED NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `group_id` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 3),
(7, 7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_division`
--

CREATE TABLE `vacancy_division` (
  `id` int NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0 ',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy_division`
--

INSERT INTO `vacancy_division` (`id`, `name`, `status`, `created_date`, `updated_date`) VALUES
(1, 'PROGRAMMER', 1, '2021-12-29 01:38:34', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vacancy_division_id` (`vacancy_division_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `applicant_file`
--
ALTER TABLE `applicant_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicant_id` (`applicant_id`);

--
-- Indexes for table `evaluator_tests`
--
ALTER TABLE `evaluator_tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `test_type_id` (`test_type_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_type_id` (`test_type_id`),
  ADD KEY `question_type_id` (`question_type_id`),
  ADD KEY `correct_answers_id` (`correct_answers_id`);

--
-- Indexes for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `correct_answers` (`correct_answers`);

--
-- Indexes for table `question_file`
--
ALTER TABLE `question_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `question_image`
--
ALTER TABLE `question_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `question_type`
--
ALTER TABLE `question_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_answers`
--
ALTER TABLE `test_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `test_transaction_id` (`test_transaction_id`);

--
-- Indexes for table `test_assessment`
--
ALTER TABLE `test_assessment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `test_transaction_id_2` (`test_transaction_id`,`question_type_id`),
  ADD KEY `question_type_id` (`question_type_id`);

--
-- Indexes for table `test_groups`
--
ALTER TABLE `test_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vacancy_division_id` (`vacancy_division_id`),
  ADD KEY `test_type_id` (`test_type_id`);

--
-- Indexes for table `test_transaction`
--
ALTER TABLE `test_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicant_id` (`applicant_id`),
  ADD KEY `test_type_id` (`test_type_id`);

--
-- Indexes for table `test_type`
--
ALTER TABLE `test_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `vacancy_division`
--
ALTER TABLE `vacancy_division`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `applicant_file`
--
ALTER TABLE `applicant_file`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `evaluator_tests`
--
ALTER TABLE `evaluator_tests`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `question_answers`
--
ALTER TABLE `question_answers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- AUTO_INCREMENT for table `question_file`
--
ALTER TABLE `question_file`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_image`
--
ALTER TABLE `question_image`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `question_type`
--
ALTER TABLE `question_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `test_answers`
--
ALTER TABLE `test_answers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `test_assessment`
--
ALTER TABLE `test_assessment`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `test_groups`
--
ALTER TABLE `test_groups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `test_transaction`
--
ALTER TABLE `test_transaction`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `test_type`
--
ALTER TABLE `test_type`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vacancy_division`
--
ALTER TABLE `vacancy_division`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `applicant`
--
ALTER TABLE `applicant`
  ADD CONSTRAINT `applicant_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `applicant_ibfk_2` FOREIGN KEY (`vacancy_division_id`) REFERENCES `vacancy_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `applicant_file`
--
ALTER TABLE `applicant_file`
  ADD CONSTRAINT `applicant_file_ibfk_1` FOREIGN KEY (`applicant_id`) REFERENCES `applicant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluator_tests`
--
ALTER TABLE `evaluator_tests`
  ADD CONSTRAINT `evaluator_tests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evaluator_tests_ibfk_2` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `question_ibfk_2` FOREIGN KEY (`correct_answers_id`) REFERENCES `question_answers` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD CONSTRAINT `question_answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_file`
--
ALTER TABLE `question_file`
  ADD CONSTRAINT `question_file_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_image`
--
ALTER TABLE `question_image`
  ADD CONSTRAINT `question_image_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_answers`
--
ALTER TABLE `test_answers`
  ADD CONSTRAINT `test_answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_answers_ibfk_2` FOREIGN KEY (`test_transaction_id`) REFERENCES `test_transaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_assessment`
--
ALTER TABLE `test_assessment`
  ADD CONSTRAINT `test_assessment_ibfk_1` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD CONSTRAINT `test_assessment_ibfk_2` FOREIGN KEY (`test_transaction_id`) REFERENCES `test_transaction` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT;

--
-- Constraints for table `test_groups`
--
ALTER TABLE `test_groups`
  ADD CONSTRAINT `test_groups_ibfk_1` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_groups_ibfk_2` FOREIGN KEY (`vacancy_division_id`) REFERENCES `vacancy_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_transaction`
--
ALTER TABLE `test_transaction`
  ADD CONSTRAINT `test_transaction_ibfk_1` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_transaction_ibfk_2` FOREIGN KEY (`applicant_id`) REFERENCES `applicant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
