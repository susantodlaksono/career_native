<?php

class Model_hasil_test extends CI_Model {

	public function hasil_test(){
        $this->db->select('b.id as vacancy_id, b.name as vacancy_name, a.*, date_format(a.created_date, "%d %b %Y") as created');
        $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
        $this->db->join('users as c', 'a.user_id = c.id');
        $this->db->where('c.active', 1);
        $this->db->order_by('a.created_date', 'desc');
        return $this->db->get('applicant as a')->result_array();
	}

    public function profil_pelamar($id){
        $this->db->select('a.*, b.name as vacancy_name');
        $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
        $this->db->where('a.id', $id);
        return $this->db->get('applicant as a')->row_array();
    }

    public function summary($id, $vacancyid){
        $daftar_test = $this->daftar_test($vacancyid);

        foreach ($daftar_test as $k => $v) {
            $test_transaction_id = $this->transaksi($id, $v['test_type_id']);
            $result[$v['test_type_id']]['name'] = $v['name'];
            $result[$v['test_type_id']]['result'] = $test_transaction_id ? $this->hasil($test_transaction_id['id'], $v['test_type_id']) : FALSE;
        }
        return $result;
    }

    public function daftar_test($vacancy_id){
        $this->db->select('a.test_type_id, b.name');
        $this->db->join('test_type as b', 'a.test_type_id = b.id');
        $this->db->where('a.vacancy_division_id', $vacancy_id);
        $this->db->order_by('a.sort', 'asc');
        $rs = $this->db->get('test_groups as a');
        return $rs->num_rows() > 0 ? $rs->result_array() : FALSE;
    }

    public function transaksi($applicant_id, $test_type_id){
        $this->db->select('id');
        $this->db->where('applicant_id', $applicant_id);
        $this->db->where('test_type_id', $test_type_id);
        $rs = $this->db->get('test_transaction');
        return $rs->num_rows() > 0 ? $rs->row_array() : FALSE;
    }

    public function hasil($test_transaction_id, $test_type_id){
        $group_question = $this->grup_pertanyaan($test_type_id);
        if($group_question->num_rows() > 0){
            foreach ($group_question->result_array() as $v) {
                $data[$v['id']]['question_type_id'] = $v['id'];
                $data[$v['id']]['question_type_name'] = $v['question_type_name'];
                if($v['id'] !== 7 || $v['id'] !== 8){
                    $total_pertanyaan = $this->total_pertanyaan($v['id'], $test_type_id);
                    $hasil = $this->jawaban_benar($test_transaction_id, $v['id'], $test_type_id);

                    $data[$v['id']]['test_transaction_id'] = $test_transaction_id;
                    $data[$v['id']]['total_question'] = $total_pertanyaan;
                    $data[$v['id']]['result'] = $hasil;
                    $data[$v['id']]['assessment'] = $this->lihat_penilaian($test_transaction_id, $v['id']);
                }
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function lihat_penilaian($trans_id, $qid){
        $this->db->select('assessment');
        $this->db->where('test_transaction_id', $trans_id);
        $this->db->where('question_type_id', $qid);
        $result = $this->db->get('test_assessment')->row_array();
        if($result){
            return $result['assessment'];
        }else{
            return NULL;
        }
    }
	
    public function grup_pertanyaan($test_type_id){
        $this->db->select('b.id, b.name as question_type_name');
        $this->db->join('question_type as b', 'a.question_type_id = b.id');
        $this->db->where('a.test_type_id', $test_type_id);
        $this->db->group_by('a.question_type_id');
        return $this->db->get('question as a');

    }

    public function total_pertanyaan($question_type_id, $test_type_id){
        $this->db->where('test_type_id', $test_type_id);
        $this->db->where('question_type_id', $question_type_id);
        return $this->db->count_all_results('question');
    }

    public function jawaban_benar($test_transaction_id, $question_type_id, $test_type_id){
        $question_id = $this->pertanyaan_id($question_type_id, $test_type_id);
        if($question_type_id == 1 || $question_type_id == 4){
            $this->db->where_in('question_id', $question_id);
            $this->db->where('test_transaction_id', $test_transaction_id);
            $this->db->where('result', 1);
            return $this->db->count_all_results('test_answers');
        }
        if($question_type_id == 2 || $question_type_id == 3 || $question_type_id == 5 || $question_type_id == 6){       
            $this->db->select('SUM(result) as total');
            $this->db->where_in('question_id', $question_id);
            $this->db->where('test_transaction_id', $test_transaction_id);
            $rs = $this->db->get('test_answers')->row_array();
            return $rs['total'];
        }
        if($question_type_id == 7){
            $_i = $this->count_psikotest($question_id, $test_transaction_id, 'I');
            $_e = $this->count_psikotest($question_id, $test_transaction_id, 'E');
            $_s = $this->count_psikotest($question_id, $test_transaction_id, 'S');
            $_n = $this->count_psikotest($question_id, $test_transaction_id, 'N');
            $_t = $this->count_psikotest($question_id, $test_transaction_id, 'T');
            $_f = $this->count_psikotest($question_id, $test_transaction_id, 'F');
            $_j = $this->count_psikotest($question_id, $test_transaction_id, 'J');
            $_p = $this->count_psikotest($question_id, $test_transaction_id, 'P');
            
            $_ie = $_i > $_e ? 'I' : 'E';
            $_sn = $_s > $_n ? 'S' : 'N';
            $_tf = $_t > $_f ? 'T' : 'F';
            $_jp = $_j > $_p ? 'J' : 'P';

            return $_ie.$_sn.$_tf.$_jp;
        }
    }

    public function pertanyaan_id($question_type_id, $test_type_id){
        $this->db->select('id');
        $this->db->where('test_type_id', $test_type_id);
        $this->db->where('question_type_id', $question_type_id);
        $rs = $this->db->get('question')->result_array();
        foreach ($rs as $key => $value) {
            $data[] = $value['id'];
        }
        return $data;
    }

    public function get_pertanyaan($testid, $transid, $qid){
        $this->db->select('a.*');
        $this->db->select('b.file, b.temp_name as temp_name_file');
        $this->db->select('c.image, c.temp_name as temp_name_image');
        $this->db->join('question_file as b', 'a.id = b.question_id', 'left');
        $this->db->join('question_image as c', 'a.id = c.question_id', 'left');
        $this->db->where('a.test_type_id', $testid);
        $this->db->where('a.question_type_id', $qid);
        $result = $this->db->get('question as a')->result_array();
        if($result){
            foreach ($result as $key => $value) {
                $data[$key] = $value;
                $data[$key]['jawaban_pelamar'] = $this->get_jawaban_pelamar($value['id'], $transid);
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function get_jawaban_pelamar($qid, $transid){
        $this->db->where('question_id', $qid);
        $this->db->where('test_transaction_id', $transid);
        $result = $this->db->get('test_answers')->row_array();
        if($result){
            return $result;
        }else{
            return FALSE;
        }
    }
}