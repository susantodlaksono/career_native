<?php

class Dashboard extends CI_Model {

	public function summary_lowongan(){
        $this->db->select('b.name as vacancy_name, count(a.vacancy_division_id) as total');
        $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
        $this->db->group_by('a.vacancy_division_id');
        return $this->db->get('applicant as a')->result_array();
    }

    public function summary_tingkat(){
        $this->db->select('a.education_degree, count(a.education_degree) as total');
        $this->db->group_by('a.education_degree');
        return $this->db->get('applicant as a')->result_array();
    }

    public function summary_kelamin(){
        $this->db->select('a.gender, count(a.gender) as total');
        $this->db->group_by('a.gender');
        return $this->db->get('applicant as a')->result_array();
    }

    public function detail_applicant($user_id, $fields = '*'){
        $this->db->select($fields);
        $this->db->where('user_id', $user_id);
        $rs = $this->db->get('applicant');
        if($fields == '*'){
            return $rs->row_array();
        }else{
            $row = $rs->row_array();
            return $row[$fields];
        }
    }

    public function detail_test($test_id, $fields = '*'){
        $this->db->select($fields);
        $this->db->where('id', $test_id);
        $rs = $this->db->get('test_type');
        if($fields == '*'){
            return $rs->row_array();
        }else{
            $row = $rs->row_array();
            return $row[$fields];
        }
    }

    private function _checking_registered($test_id, $applicant_id){
        $this->db->where('test_type_id', $test_id);
        $this->db->where('applicant_id', $applicant_id);
        $rs = $this->db->get('test_transaction');
        if($rs->num_rows() > 0){ 
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function rules_vacancy($test_id, $user_id){
        $vacancy_division_id = $this->detail_applicant($user_id, 'vacancy_division_id');
        $this->db->where('vacancy_division_id', $vacancy_division_id);
        $this->db->where('test_type_id', $test_id);
        $rs = $this->db->get('test_groups');
        if($rs->num_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }   
    }

    public function rules_transaction($test_id, $user_id){
        $applicant_id = $this->detail_applicant($user_id, 'id');
        $this->db->where('applicant_id', $applicant_id);
        $this->db->where('test_type_id', $test_id);
        $this->db->where('status', 3);
        $rs = $this->db->get('test_transaction');
        //Jika test ditemukan dengan status Finish
        if($rs->num_rows() > 0){ 
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function registration_test($test_id, $user_id, $time_now_js){
        $applicant_id = $this->detail_applicant($user_id, 'id');
        $time_test = $this->detail_test($test_id, 'time') * 60;
        $registered = $this->_checking_registered($test_id, $applicant_id);
        if(!$registered){
            $start_time = strtotime($time_now_js);
            $end_time = $start_time + $time_test;

            $insert_entity = array(
                'time_start' => date('Y-m-d H:i:s', $start_time),
                'time_end' => date('Y-m-d H:i:s', $end_time),
                'applicant_id' => $applicant_id,
                'test_type_id' => $test_id,
                'status' => 1

            );  
            return $this->db->insert('test_transaction', $insert_entity);
        }else{
            return TRUE;
        }
    }

}