<?php

class Model_lowongan extends CI_Model {

	public function group_test($id){
		$this->db->select('a.id, a.test_type_id as test_type_id, a.rules, a.random_test, a.sort, b.name, a.status');
        $this->db->join('test_type as b', 'a.test_type_id = b.id');
        $this->db->where('a.vacancy_division_id', $id);
        $this->db->order_by('a.sort', 'asc');
        return $this->db->get('test_groups as a')->result_array();
	}
	
}