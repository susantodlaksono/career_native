<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Model Result
 *
 * @author SUSANTO DWI LAKSONO
 */

class Result extends CI_Model {

	public function get_applicant($for, $params) {
        $this->db->select('a.test_type_id, b.*, c.name as vacancy_name');
        $this->db->join('applicant as b', 'a.applicant_id = b.id');
        $this->db->join('vacancy_division as c', 'b.vacancy_division_id = c.id');
        $this->db->where('a.status', 2);
        $this->db->where('a.test_type_id', $params['test_type_id']);
        $this->db->where('date(a.time_start) between "'.$params['sdate'].'" and "'.$params['edate'].'"');
        $this->db->order_by('a.id', 'asc');
        if ($for == 'get') {
            $rs = $this->db->get('test_transaction as a', 10, $params['offset'])->result_array();
            if($rs){
	            foreach ($rs as $key => $value) {
	            	$data[$value['id']]['name'] = $value['name'];
	            	$data[$value['id']]['email'] = $value['email'];
	            	$data[$value['id']]['vacancy_name'] = $value['vacancy_name'];
	            	$data[$value['id']]['age'] = $value['age'];
	            	$data[$value['id']]['contact_number'] = $value['contact_number'];
	            	$data[$value['id']]['education_degree'] = $value['education_degree'];
	            	$data[$value['id']]['gender'] = $value['gender'];
	            }
	            return $data;
            }else{
            	return NULL;
            }
            
        } else if ($for == 'count') {
            return $this->db->count_all_results('test_transaction as a');
        }
    }

    // public function mapping_result($test_type_id){
    // 	$group_question_type = $this->group_question_type($test_type_id);
    // 	if($group_question_type){
    // 		foreach ($group_question_type as $v) {
    // 			switch ($v['question_type_id']) {
		  //   		case '1':
		  //   			$data[$v['question_type_id']]['correct_answers'] = $this->correct_answers('')
		  //   			break;
		    		
		  //   		default:
		  //   			# code...
		  //   			break;
		  //   	}		
    // 		}
    // 	}else{
    // 		return FALSE;
    // 	}
    // }

    // public function group_question_type($test_type_id){
    // 	$this->db->select('question_type_id');
    // 	$this->db->where('test_type_id', $test_type_id);
    // 	$this->db->group_by('question_type_id');
    // 	return $this->db->get('question')->result_array();
    // }

}