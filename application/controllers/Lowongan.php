<?php

class Lowongan extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_lowongan');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Lowongan',
            'datalowongan' => $this->db->order_by('id', 'DESC')->get('vacancy_division')->result_array(),
            'content' => 'themes/pages/admin/page/lowongan/lowongan'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function tambah(){
        $data = array(
            'title' => 'Tambah Lowongan',
            'content' => 'themes/pages/admin/page/lowongan/tambah_lowongan'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function detail($id){
        $test_groups = $this->db->select('a.id, a.test_type_id as test_type_id, a.rules, a.random_test, a.sort, b.name, a.status')
                                                ->join('test_type as b', 'a.test_type_id = b.id')
                                                ->where('a.vacancy_division_id', $id)
                                                ->order_by('a.sort', 'asc')
                                                ->get('test_groups as a')->result_array();
        $data = array(
            'title' => 'Detail Lowongan',
            'group_test' => $this->model_lowongan->group_test($id),
            'test_groups' => $test_groups,
            'id' => $id,
            'test_type_list' => $this->db->select('id, name')->where('status', 1)->get('test_type')->result_array(),
            'lowongan' => $this->db->get_where('vacancy_division', array('id' => $id))->row_array(),
            'content' => 'themes/pages/admin/page/lowongan/detail_lowongan'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function insert(){
        $post = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama lowongan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_message('required', '{field} wajib diisi');
        if($this->form_validation->run()){
            $entity = array(
                'name' => $post['name'],
                'status' => $post['status'],
                'created_date' => date('Y-m-d H:i:s'),
            );
            $insert = $this->db->insert('vacancy_division', $entity);
            if($insert){
                $this->session->set_userdata('notif', 'Lowongan berhasil di tambahkan');
                redirect('lowongan', 'refresh');
            }else{
                $this->session->set_userdata('notif', 'Lowongan gagal di tambahkan');
                redirect('lowongan/tambah', 'refresh');
            }
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('lowongan/tambah', 'refresh');
        }
    }

    public function tambah_test(){
        $post = $this->input->post();
        $check = $this->db->where('vacancy_division_id', $post['id'])->where('test_type_id', $post['test_id'])->get('test_groups');
        if($check->num_rows() == 0){
            $this->db->insert('test_groups', array('vacancy_division_id' => $post['id'], 'test_type_id' => $post['test_id'], 'status' => 1));
            $this->session->set_userdata('notif', 'Test berhasil di daftarkan ');
            redirect('lowongan/detail/'.$post['id'].'', 'refresh');
        }else{
            $this->session->set_userdata('notif', 'Test ini sudah terdaftar');
            redirect('lowongan/detail/'.$post['id'].'', 'refresh');
        }
    }

    public function update(){
        $post = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama Lowongan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_message('required', '{field} wajib diisi');
        if($this->form_validation->run()){
            $entity = array(
                'name' => $post['name'],
                'status' => $post['status']
            );
            $insert = $this->db->update('vacancy_division', $entity, array('id' => $post['id']));
            if($insert){
                $this->session->set_userdata('notif', 'Lowongan berhasil di perbaharui');
                redirect('lowongan/detail/'.$post['id'].'', 'refresh');
            }else{
                $this->session->set_userdata('notif', 'Lowongan gagal di perbaharui');
                redirect('lowongan/detail/'.$post['id'].'', 'refresh');
            }
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('lowongan/detail/'.$post['id'].'', 'refresh');
        }
    }

    public function simpanKonfigurasi(){
        $post = $this->input->post();
        if(isset($post['rules']) && isset($post['sort'])){
            foreach ($post['rules'] as $key => $value) {
                $this->db->update('test_groups', array('rules' => $value['vacancy_id']), array('id' => $key));
            }
            foreach ($post['sort'] as $key => $value) {
                $this->db->update('test_groups', array('sort' => $value['sortdata']), array('id' => $key));
            }
            $this->session->set_userdata('notif', 'Konfigurasi test berhasil di perbaharui');
            redirect('lowongan/detail/'.$post['vacancy_rules_id'].'', 'refresh');
        }else{
            $this->session->set_userdata('notif', 'Tidak ada data yang dirubah');
            redirect('lowongan/detail/'.$post['vacancy_rules_id'].'', 'refresh');
        }
    }
}