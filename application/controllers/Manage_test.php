<?php

class Manage_test extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_test');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Daftar Test',
            'test' => $this->db->get('test_type')->result_array(),
            'content' => 'themes/pages/admin/page/tests'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function tambah(){
        $data = array(
            'title' => 'Tambah Test',
            'content' => 'themes/pages/admin/page/tambah_test'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function insert(){
        $post = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama Test', 'required');
        $this->form_validation->set_rules('mode_test', 'Jawaban Lengkap', 'required');
        $this->form_validation->set_rules('total_question', 'Total Pertanyaan', 'required');
        $this->form_validation->set_rules('times', 'Waktu', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_message('required', '{field} wajib diisi');
        if($this->form_validation->run()){
            $guidetest = isset($post['guidetest']) && $post['guidetest'] != '' ? $post['guidetest'] : NULL;
            $entity = array(
                'name' => $post['name'],
                'total_question' => $post['total_question'],
                'mode_test' => $post['mode_test'],
                'time' => $post['times'],
                'status' => $post['status'] == 1 ? 1 : NULL,
                'description' => $guidetest
            );
            $insert = $this->db->insert('test_type', $entity);
            if($insert){
                $this->session->set_userdata('notif', 'Test berhasil di tambahkan');
                redirect('manage_test', 'refresh');
            }else{
                $this->session->set_userdata('notif', 'test gagal di tambahkan');
                redirect('manage_test/tambah', 'refresh');
            }
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('manage_test/tambah', 'refresh');
        }
    }

    public function update(){
        $post = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama Test', 'required');
        $this->form_validation->set_rules('mode_test', 'Jawaban Lengkap', 'required');
        $this->form_validation->set_rules('total_question', 'Total Pertanyaan', 'required');
        $this->form_validation->set_rules('times', 'Waktu', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_message('required', '{field} wajib diisi');
        if($this->form_validation->run()){
            $guidetest = isset($post['guidetest']) && $post['guidetest'] != '' ? $post['guidetest'] : NULL;
            $entity = array(
                'name' => $post['name'],
                'total_question' => $post['total_question'],
                'mode_test' => $post['mode_test'],
                'time' => $post['times'],
                'status' => $post['status'] == 1 ? 1 : NULL,
                'description' => $guidetest
            );
            $update = $this->db->update('test_type', $entity, array('id' => $post['id']));
            if($update){
                $this->session->set_userdata('notif', 'Test berhasil di perbaharui');
                redirect('manage_test', 'refresh');
            }else{
                $this->session->set_userdata('notif', 'test gagal di perbaharui');
                redirect('manage_test/detail/'.$post['id'].'', 'refresh');
            }
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('manage_test/detail/'.$post['id'].'', 'refresh');
        }
    }

    public function detail($id){
        $data = array(
            'title' => 'Detail Test',
            'test' => $result = $this->db->get_where('test_type', array('id' => $id))->row_array(),
            'content' => 'themes/pages/admin/page/detail_test'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function status($id, $status){
        $tmp['status'] = $status;
        $where['id'] = $id;
        $result = $this->db->update('test_type', $tmp, $where);
        if($result){
            $this->session->set_userdata('notif', 'Status test berhasil di perbaharui');
        }else{
            $this->session->set_userdata('notif', 'Status test gagal di perbaharui');
        }
        redirect('manage_test', 'refresh');
    }

    public function hapus($id){
        $result = $this->db->delete('test_type', array('id' => $id));
        if($result){
            $this->session->set_userdata('notif', 'Test berhasil di hapus');
        }else{
            $this->session->set_userdata('notif', 'Test gagal di hapus');
        }
        redirect('manage_test', 'refresh');
    }
}