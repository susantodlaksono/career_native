<?php

class Penilai extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Penilai',
            'evaluator' => $this->getPenilai(),
            'content' => 'themes/pages/admin/page/evaluator/list'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function getPenilai(){
        $this->db->select('a.test_type_id, a.id, b.name as test_name, group_concat(c.first_name) as evaluator');
        $this->db->join('test_type as b', 'a.test_type_id = b.id');
        $this->db->join('users  as c', 'a.user_id  = c.id');
        $this->db->group_by('a.test_type_id');
        return $this->db->get('evaluator_tests as a')->result_array();
    }

    public function tambah(){
        $data = array(
            'title' => 'Tambah Penilai',
            'evaluator' => $this->getPenilai(),
            'content' => 'themes/pages/admin/page/evaluator/tambah'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function edit($testtypeid){
        $listid = array();
        $result = $this->db->select('b.id')->join('users as b', 'a.user_id = b.id')
                            ->where('a.test_type_id', $testtypeid)
                            ->get('evaluator_tests as a')->result_array();
        if($result){
            foreach ($result as $v) {
                $listid[] = (int) $v['id'];
            }
        }
        $data = array(
            'title' => 'Edit Penilai',
            'listid' => $listid,
            'testtypeid' => $testtypeid,
            'content' => 'themes/pages/admin/page/evaluator/edit'
        );        
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function simpan(){
        $post = $this->input->post();
        $check = $this->db->where('test_type_id', $post['test_type'])->get('evaluator_tests');
        if($check->num_rows() > 0){
            $this->session->set_userdata('notif', 'Tipe test telah terdaftar');
            redirect('penilai/tambah', 'refresh');
        }else{
            if($post['choose_evaluator']){
                foreach ($post['choose_evaluator'] as $key => $value) {
                    $entity = array(
                        'user_id' => $value,
                        'test_type_id' => $post['test_type']
                    );
                    $insert = $this->db->insert('evaluator_tests', $entity);
                }
                $this->session->set_userdata('notif', 'Penilai berhasil ditambahkan');
                redirect('penilai/tambah', 'refresh');
            }else{
                $this->session->set_userdata('notif', 'Tidak ada penilai yang dipilih');
                redirect('penilai/tambah', 'refresh');
            }
        }
    }

    public function update(){
        $post = $this->input->post();
        $this->db->delete('evaluator_tests', array('test_type_id' => $post['test_type']));
        if($post['choose_evaluator']){
            foreach ($post['choose_evaluator'] as $key => $value) {
                $entity = array(
                    'user_id' => $value,
                    'test_type_id' => $post['test_type']
                );
                $insert = $this->db->insert('evaluator_tests', $entity);
            }
            $this->session->set_userdata('notif', 'Penilai berhasil perbaharui');
            redirect('penilai/edit/'.$post['test_type'].'', 'refresh');
        }else{
            $this->session->set_userdata('notif', 'Tidak ada penilai yang dipilih');
            redirect('penilai/edit/'.$post['test_type'].''  , 'refresh');
        }
    }

    public function hapus($id){
        $this->db->delete('evaluator_tests', array('id' => $id));
        $this->session->set_userdata('notif', 'Data berhasil di hapus');
        redirect('penilai', 'refresh');
    }
}