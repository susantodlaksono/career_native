<?php

class Pengguna extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Pengguna',
            'pengguna' => $this->getPengguna(),
            'content' => 'themes/pages/admin/page/pengguna/list'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function getPengguna(){
        $this->db->select('a.*');
        $this->db->select('GROUP_CONCAT(b.group_id SEPARATOR ",") as role_group_id');
        $this->db->select('GROUP_CONCAT(f.name SEPARATOR ",") as role_group');

        $this->db->join('users_groups as b', 'a.id = b.user_id', 'left');
        $this->db->join('groups as f', 'b.group_id = f.id', 'left');    

        $this->db->where_in('b.group_id', array(1,3));

        $this->db->group_by('a.id');
        $this->db->order_by('a.id', 'desc');
        return $this->db->get('users as a')->result_array();
    }

    public function tambah(){
        $data = array(
            'title' => 'Tambah Pengguna',
            'role' => $this->db->where_in('id', array(1,3))->get('groups')->result_array(),
            'content' => 'themes/pages/admin/page/pengguna/tambah'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function edit($id){
        $data = array(
            'title' => 'Edit Pengguna',
            'role' => $this->db->where_in('id', array(1,3))->get('groups')->result_array(),
            'user_group' => $this->db->where('user_id', $id)->get('users_groups')->row_array(),
            'user' => $this->db->where('id', $id)->get('users')->row_array(),
            'content' => 'themes/pages/admin/page/pengguna/edit'
        );        
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function simpan(){
        $params = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'Nama', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run()){
            $user['first_name'] = $params['first_name'];
            $this->ion_auth->register($params['username'], $params['password'], $params['email'], $user, $params['role']);
            $this->session->set_userdata('notif', 'Pengguna berhasil ditambahkan');
            redirect('pengguna/tambah'  , 'refresh');
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('pengguna/tambah'  , 'refresh');
        }
    }

    public function update(){
        $params = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'Nama', 'required');
        if($params['email'] != $params['email_before']){
            $this->form_validation->set_rules('email', 'Email ', 'required|valid_email|is_unique[users.email]');
        }else{
            $this->form_validation->set_rules('email', 'Email ', 'required|valid_email');
        }
        if($params['username'] != $params['username_before']){
            $this->form_validation->set_rules('username', 'Username ', 'required|is_unique[users.username]');
        }else{
            $this->form_validation->set_rules('username', 'Username ', 'required');
        }
        if($this->form_validation->run()){
            $user['first_name'] = $params['first_name'];
            $user['username'] = $params['username'];
            $user['email'] = $params['email'];
            $user['active'] = $params['active'];
            if($params['password'] != ''){
                $user['password'] = $params['password'];
            }
            $this->ion_auth->update($params['id'], $user);
            $this->ion_auth->remove_from_group(false, $params['id']);
            $this->ion_auth->add_to_group($params['role'], $params['id']);            

            $this->session->set_userdata('notif', 'Pengguna berhasil diperbaharui');
            redirect('pengguna'  , 'refresh');
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('pengguna/edit/'.$params['id'].''  , 'refresh');
        }
    }

    public function hapus($id){
        $this->ion_auth->delete_user($id);
        $this->session->set_userdata('notif', 'Pengguna berhasil di hapus');
        redirect('pengguna', 'refresh');
    }
}