<?php

class Pelamar extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_pelamar');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Pelamar',
            'datapelamar' => $this->model_pelamar->getdata(),
            'content' => 'themes/pages/admin/page/pelamar'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function status($userid, $status){
        $tmp['active'] = $status;
        $where['id'] = $userid;
        $result = $this->db->update('users', $tmp, $where);
        if($result){
            $this->session->set_userdata('notif', 'Status pelamar berhasil di perbaharui');
        }else{
            $this->session->set_userdata('notif', 'Status pelamar gagal di perbaharui');
        }
        redirect('pelamar', 'refresh');
    }

    public function hapus($userid, $applicantid){
        $file = $this->db->where('applicant_id', $applicantid)->get('applicant_file')->result_array();
        if($file){
            foreach ($file as $v) {
                if($v['applicant_type_file'] == 1){
                    if (file_exists('files/pelamar_cv/'.$v['applicant_file'].'')){
                        unlink('files/pelamar_cv/'.$v['applicant_file'].'');
                    }
                }
                if($v['applicant_type_file'] == 2){
                    if (file_exists('files/pelamar_pasphoto/'.$v['applicant_file'].'')){
                        unlink('files/pelamar_pasphoto/'.$v['applicant_file'].'');
                    }
                }
            }
        }
        $result = $this->db->delete('users', array('id' => $userid));
        if($result){
            $this->session->set_userdata('notif', 'Pelamar berhasil di hapus');
        }else{
            $this->session->set_userdata('notif', 'Pelamar gagal di hapus');
        }
        redirect('pelamar', 'refresh');
    }
}