<?php

class Hasil_test extends MY_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('model_hasil_test');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$get = $this->input->get();
    	$data = array(
            'title' => 'Hasil Test',
            'hasil_test' => $this->model_hasil_test->hasil_test(),
            'content' => 'themes/pages/admin/page/hasil_test',
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function detail($id, $vacancyid){
    	$data = array(
            'title' => 'Detail Hasil Test',
            'pelamar' => $this->model_hasil_test->profil_pelamar($id),
            'hasil_test' => $this->model_hasil_test->summary($id, $vacancyid),
            'id' => $id,
            'vacancyid' => $vacancyid,
            'content' => 'themes/pages/admin/page/hasil_test_detail',
        );
        // echo json_encode($data['hasil_test']);exit;
        $this->load->view('themes/pages/admin/index', $data);	
    }

    public function penilaian($testid, $transid, $qid, $id, $vacancyid){
    	$data = array(
            'title' => 'Detail Penilaian',
            'q_type_id' => $qid,
            'trans_id' => $transid,
            'test_id' => $testid,
            'id' => $id,
            'vacancyid' => $vacancyid,
            'pertanyaan' => $this->model_hasil_test->get_pertanyaan($testid, $transid, $qid),
            'content' => 'themes/pages/admin/page/hasil_test_penilaian',
        );
        $this->load->view('themes/pages/admin/index', $data);	
    }

    public function status_lolos(){
        $ins = array(
            'status' => $this->_post['status'] ? $this->_post['status'] : NULL
        );
        $this->db->update('applicant', $ins, array('id' => $this->_post['applicant_id']));
        $this->session->set_userdata('notif', 'Status pelamar berhasil di perbaharui');
        redirect('hasil_test/detail/'.$this->_post['id'].'/'.$this->_post['vacancyid']);
    }

    public function beri_penilaian(){
        $nilai = array_sum($this->_post['nilai']);
        $total = count($this->_post['nilai']);
        $assessment = ($nilai / $total) * 10;

        foreach ($this->_post['nilai'] as $k => $v) {
            if($v != ''){
                $ins = array(
                    'result' => $v
                );
                $this->db->update('test_answers', $ins, array('id' => $k));
            }
        }
        $test_assessment = $this->db->where('test_transaction_id', $this->_post['trans_id'])->where('question_type_id', $this->_post['q_type_id'])->get('test_assessment')->row_array();
        if($test_assessment){
            $ins = array(
                'assessment' => $assessment
            );
            $this->db->update('test_assessment', $ins, array('id' => $test_assessment['id']));
        }else{
            $ins = array(
                'test_transaction_id' => $this->_post['trans_id'],
                'question_type_id' => $this->_post['q_type_id'],
                'assessment' => $assessment
            );
            $this->db->insert('test_assessment', $ins);
        }
        $this->session->set_userdata('notif', 'Penilaian test berhasil di perbaharui');
        redirect('hasil_test/detail/'.$this->_post['id'].'/'.$this->_post['vacancyid']);

    }

    public function detail_psikotest(){
        $this->load->library('psikotes');
        $response['description'] = $this->psikotes->description($this->_get['code']);
        $this->json_result($response);
    }
}