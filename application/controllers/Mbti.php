<?php

class Mbti extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

 	public function index(){
        $listsoal = $this->soalSkalaMBTI();
        $result = $this->jawabanSkalaMBTI($listsoal);        
        $data = array(
            'title' => 'Psikotest MBTI',
            'result' => $result,
            'content' => 'themes/pages/admin/page/mbti/list'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function soalSkalaMBTI(){
        $this->db->where('test_type_id', 1);
        $result = $this->db->get('question')->result_array();
        if($result){
            foreach ($result as $v) {
                $data[] = $v['id'];
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function jawabanSkalaMBTI($list){
        if($list){
            $this->db->where_in('question_id', $list);
            $this->db->order_by('question_id', 'asc');
            $result = $this->db->get('question_answers')->result_array();
            $grouped_types = array();

            foreach($result as $type){
                $grouped_types[$type['question_id']][] = $type;
            }
            return $grouped_types;
        }else{
            return FALSE;
        }
    }

    public function edit($id){
        $result = $this->db->where('question_id', $id)->order_by('id', 'asc')->get('question_answers')->result_array();
        $data = array(
            'title' => 'Edit Pernyataan',
            'result' => $result,
            'content' => 'themes/pages/admin/page/mbti/edit'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function update(){
        foreach ($this->_post['data'] as $key => $value) {
            $tmp = array(
                'question_answers' => $value['question_answers'],
                'question_answers_alias' => $value['question_answers_alias'],
            );
            $this->db->update('question_answers', $tmp, array('id' => $key));
        }
        $this->session->set_userdata('notif', 'Data berhasil di perbaharui');
        redirect('mbti', 'refresh'); 
    }


}