<?php

class Main extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
        $role_id = $this->db->select('group_id')->where('user_id', $this->_user->id)->get('users_groups')->row_array();
        if($this->_role_id['group_id'] == 1){
        	$data = array(
                'title' => 'Dashboard',
                'content' => 'themes/pages/admin/page/dashboard',
                'vacancy' => $this->dashboard->summary_lowongan(),
                'tingkat' => $this->dashboard->summary_tingkat(),
                'kelamin' => $this->dashboard->summary_kelamin(),
                'pelamar' => $this->db->count_all_results('applicant'),
                'active' => $this->db->where('active', 1)->count_all_results('users'),
                'inactive' => $this->db->where('active', 0)->count_all_results('users'),
        	);
            $this->load->view('themes/pages/admin/index', $data);
        }else if($this->_role_id['group_id'] == 2){

            $pelamar = $this->profilPelamar($this->_user->id);
            $vacancy = $this->getVacancy($this->_user->id);
            $test_list = $this->getTestList($vacancy['id'], $vacancy['vacancy_division_id']);

            $data = array(
                'title' => 'Dashboard',
                'pelamar' => $pelamar,
                'vacancy' => $vacancy,
                'test_list' => $test_list,
                'content' => 'themes/pages/pelamar/page/dashboard',
            );
            $this->load->view('themes/pages/pelamar/index', $data);
        }else if($this->_role_id['group_id'] == 3){

            $data = array(
                'title' => 'Dashboard',
                'pelamar' => $this->getDataPelamarPenilai($this->_user->id),
                'content' => 'themes/pages/penilai/dashboard',
            );
            $this->load->view('themes/pages/penilai/index', $data);
        }
    }

    public function detailTestPelamar($testid, $transid, $qid){
        $data = array(
            'title' => 'Detail Penilaian',
            'q_type_id' => $qid,
            'trans_id' => $transid,
            'test_id' => $testid,
            'pertanyaan' => $this->getPertanyaanPenilai($testid, $transid, $qid),
            'content' => 'themes/pages/penilai/hasil_test_penilaian',
        );
        $this->load->view('themes/pages/penilai/index', $data);   
    }


    public function getPertanyaanPenilai($testid, $transid, $qid){
        $this->db->select('a.*');
        $this->db->select('b.file, b.temp_name as temp_name_file');
        $this->db->select('c.image, c.temp_name as temp_name_image');
        $this->db->join('question_file as b', 'a.id = b.question_id', 'left');
        $this->db->join('question_image as c', 'a.id = c.question_id', 'left');
        $this->db->where('a.test_type_id', $testid);
        $this->db->where('a.question_type_id', $qid);
        $result = $this->db->get('question as a')->result_array();
        if($result){
            foreach ($result as $key => $value) {
                $data[$key] = $value;
                $data[$key]['jawaban_pelamar'] = $this->jawabanPelamar($value['id'], $transid);
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function jawabanPelamar($qid, $transid){
        $this->db->where('question_id', $qid);
        $this->db->where('test_transaction_id', $transid);
        $result = $this->db->get('test_answers')->row_array();
        if($result){
            return $result;
        }else{
            return FALSE;
        }
    }

    public function _evaluator_test_type($user_id){
        $this->db->where('user_id', $user_id);
        $rs = $this->db->get('evaluator_tests');
        if($rs->num_rows() > 0){
            foreach ($rs->result_array() as $key => $value) {
                $data[] = $value['test_type_id'];
            }
            return $data;
        }else{
            return FALSE;
        }
    }

    public function getDataPelamarPenilai($user_id){
        $test_type_id = $this->_evaluator_test_type($user_id);

        $this->db->select('a.*, date_format(a.time_start, "%d %b %Y") as time_start');
        $this->db->select('b.name as test_type_name');
        $this->db->select('c.name, c.education_degree');

        $this->db->join('test_type as b', 'a.test_type_id = b.id');
        $this->db->join('applicant as c', 'a.applicant_id = c.id');
        $this->db->where('a.status', 2); // finish test
        $this->db->where_in('a.test_type_id', $test_type_id);
        $this->db->order_by('a.id', 'desc');
        return $this->db->get('test_transaction as a')->result_array();
    }

    public function rubahStatusPelamar($id, $stat){
        $this->db->update('test_transaction', array('checked_transaction' => $stat), array('id' => $id));
        $this->session->set_userdata('notif', 'Status test pelamar berhasil diperbaharui');
        redirect('main', 'refresh');
    }

    public function registration_test(){
        $p = $this->input->post();
        $rules_vacancy = $this->dashboard->rules_vacancy($p['id'], $this->_user->id);
        $rules_transaction = $this->dashboard->rules_transaction($p['id'], $this->_user->id);
        if($rules_vacancy && $rules_transaction){
            $register_tests = $this->dashboard->registration_test($p['id'], $this->_user->id, date('Y-m-d H:i:s'));
            $response['time_now_js'] = $p['time_now_js'];
            $response['success'] = $register_tests ? TRUE : FALSE;
            $response['msg'] = $register_tests ? '' : 'Transaction Failed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'You dont have permission';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function simpanPenilaian(){
        $nilai = array_sum($this->_post['nilai']);
        $total = count($this->_post['nilai']);
        $assessment = ($nilai / $total) * 10;

        foreach ($this->_post['nilai'] as $k => $v) {
            if($v != ''){
                $ins = array(
                    'result' => $v
                );
                $this->db->update('test_answers', $ins, array('id' => $k));
            }
        }
        $test_assessment = $this->db->where('test_transaction_id', $this->_post['trans_id'])->where('question_type_id', $this->_post['q_type_id'])->get('test_assessment')->row_array();
        if($test_assessment){
            $ins = array(
                'assessment' => $assessment
            );
            $this->db->update('test_assessment', $ins, array('id' => $test_assessment['id']));
        }else{
            $ins = array(
                'test_transaction_id' => $this->_post['trans_id'],
                'question_type_id' => $this->_post['q_type_id'],
                'assessment' => $assessment
            );
            $this->db->insert('test_assessment', $ins);
        }
        $this->session->set_userdata('notif', 'Penilaian test berhasil di perbaharui');
        redirect('main', 'refresh');

    }

    public function profilPelamar($user_id){
        $this->db->select('a.*, b.name as vacancy_division_name');
        $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
        $this->db->where('a.user_id', $user_id);
        return $this->db->get('applicant as a')->row_array();
    }

    public function getVacancy($user_id){
        $this->db->select('vacancy_division_id, id');
        $this->db->where('user_id', $user_id);
        return $this->db->get('applicant')->row_array();
    }

    public function getTestList($vacancy_id, $vacancy_division_id){
        $this->db->select('a.rules, b.*,c.*');
        $this->db->join('test_type as b', 'a.test_type_id = b.id', 'left');
        $this->db->join('(
                        select status as status_register, test_type_id 
                        from test_transaction where applicant_id = '.$vacancy_id.'
                     ) as c', 'b.id = c.test_type_id', 'left');

        $this->db->where('a.vacancy_division_id', $vacancy_division_id);
        $this->db->where('a.status', 1);
        $this->db->where('a.random_test IS NULL');
        $this->db->order_by('a.sort', 'asc');
        return $this->db->get('test_groups as a');
    }

}