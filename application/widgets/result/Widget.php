<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        $this->load->model('applicant');
        $this->load->library('assessment');
        $this->load->library('psikotes');

        if (!$this->ion_auth->logged_in()) {
            die;
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Result Tests'
        );
        $this->render_widget($data);
    }

}