<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Mailing{

	public function __construct(){
     	$this->ci = & get_instance();
  	}

  	public function send($params){
  		$this->ci->load->library('email');
      $this->ci->email->from('robot@ebdesk.com', $params['alias']);
      foreach ($params['mail_to'] as $v) {
      	$this->ci->email->to($v);
      }
      $this->ci->email->set_mailtype('html');
      $this->ci->email->subject($params['subject']);
      $this->ci->email->message($params['body']);
      $send = $this->ci->email->send();
      if($send){
         return TRUE;
      }else{
         return FALSE;
      }
  	}

  	public function _rules($config_set){
  		switch ($config_set) {
  			case '_send_email_approval_account':
  				
  				break;
  		}
  	}

}