<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Library for Assessment result
 *
 * @author SUSANTO DWI LAKSONO
 */

class Result_assessment{

	public function __construct() {
        $this->ci = & get_instance();
    }

	public function summary_simulation($appid, $vacancy_id){
		$test_groups = $this->test_groups($vacancy_id);
    	foreach ($test_groups as $k => $v) {
    		$test_transaction_id = $this->get_transaction($appid, $v['test_type_id']);
    		$result[$v['test_type_id']]['name'] = $v['name'];
    		$result[$v['test_type_id']]['result'] = $test_transaction_id ? $this->get_result($test_transaction_id['id'], $v['test_type_id']) : FALSE;
    	}
    	return $result;
	}

    public function summary($params){
    	$test_groups = $this->test_groups($params['vacancy_id']);
    	foreach ($test_groups as $k => $v) {
    		$test_transaction_id = $this->get_transaction($params['applicant_id'], $v['test_type_id']);
    		$result[$v['test_type_id']]['name'] = $v['name'];
    		$result[$v['test_type_id']]['result'] = $test_transaction_id ? $this->get_result($test_transaction_id['id'], $v['test_type_id']) : FALSE;
    	}
    	return $result;
    }

    public function get_result($test_transaction_id, $test_type_id){
    	$group_question = $this->group_question($test_type_id);
    	if($group_question->num_rows() > 0){
    		foreach ($group_question->result_array() as $v) {
    			$data[$v['id']]['question_type_id'] = $v['id'];
    			$data[$v['id']]['question_type_name'] = $v['question_type_name'];
    			if($v['id'] !== 7 || $v['id'] !== 8){
    				$data[$v['id']]['total_question'] = $this->total_question($v['id'], $test_type_id);
    				$data[$v['id']]['result'] = $this->correct_answers($test_transaction_id, $v['id'], $test_type_id);
    			}
    		}
    		return $data;
    	}else{
    		return FALSE;
    	}
    }

    public function total_question($question_type_id, $test_type_id){
    	$this->ci->db->where('test_type_id', $test_type_id);
    	$this->ci->db->where('question_type_id', $question_type_id);
    	return $this->ci->db->count_all_results('question');
    }

    public function correct_answers($test_transaction_id, $question_type_id, $test_type_id){
    	$question_id = $this->question_id($question_type_id, $test_type_id);
    	if($question_type_id == 1 || $question_type_id == 4){
    		$this->ci->db->where_in('question_id', $question_id);
	    	$this->ci->db->where('test_transaction_id', $test_transaction_id);
	    	$this->ci->db->where('result', 100);
	    	return $this->ci->db->count_all_results('test_answers');
		}
		if($question_type_id == 2 || $question_type_id == 3 || $question_type_id == 5 || $question_type_id == 6){		
			$this->ci->db->select('SUM(result) as total');
			$this->ci->db->where_in('question_id', $question_id);
	    	$this->ci->db->where('test_transaction_id', $test_transaction_id);
	    	$rs = $this->ci->db->get('test_answers')->row_array();
	    	return $rs['total'];
		}
		if($question_type_id == 7){
			$_i = $this->count_psikotest($question_id, $test_transaction_id, 'I');
	     	$_e = $this->count_psikotest($question_id, $test_transaction_id, 'E');
	     	$_s = $this->count_psikotest($question_id, $test_transaction_id, 'S');
	     	$_n = $this->count_psikotest($question_id, $test_transaction_id, 'N');
	     	$_t = $this->count_psikotest($question_id, $test_transaction_id, 'T');
	     	$_f = $this->count_psikotest($question_id, $test_transaction_id, 'F');
	     	$_j = $this->count_psikotest($question_id, $test_transaction_id, 'J');
	     	$_p = $this->count_psikotest($question_id, $test_transaction_id, 'P');
	        
	     	$_ie = $_i > $_e ? 'I' : 'E';
	     	$_sn = $_s > $_n ? 'S' : 'N';
	     	$_tf = $_t > $_f ? 'T' : 'F';
	     	$_jp = $_j > $_p ? 'J' : 'P';

	     	return $_ie.$_sn.$_tf.$_jp;
		}
		if($question_type_id == 8){
			$this->ci->load->library('papi_kostick');
			return $this->ci->papi_kostick->counting_papi($question_id, $test_transaction_id);
		}
    }

    public function count_psikotest($question_id, $test_transaction_id, $answers){
    	$this->ci->db->where_in('question_id', $question_id);
    	$this->ci->db->where('test_transaction_id', $test_transaction_id);
    	$this->ci->db->where('answers', $answers);
    	return $this->ci->db->get('test_answers')->num_rows();
    }

    public function question_id($question_type_id, $test_type_id){
    	$this->ci->db->select('id');
    	$this->ci->db->where('test_type_id', $test_type_id);
    	$this->ci->db->where('question_type_id', $question_type_id);
    	$rs = $this->ci->db->get('question')->result_array();
    	foreach ($rs as $key => $value) {
    		$data[] = $value['id'];
    	}
    	return $data;
    }

    public function group_question($test_type_id){
		$this->ci->db->select('b.id, b.name as question_type_name');
		$this->ci->db->join('question_type as b', 'a.question_type_id = b.id');
		$this->ci->db->where('a.test_type_id', $test_type_id);
		$this->ci->db->group_by('a.question_type_id');
		return $this->ci->db->get('question as a');

    }
    public function test_groups($vacancy_id){
    	$this->ci->db->select('a.test_type_id, b.name');
    	$this->ci->db->join('test_type as b', 'a.test_type_id = b.id');
    	$this->ci->db->where('a.vacancy_division_id', $vacancy_id);
    	$this->ci->db->order_by('a.sort', 'asc');
    	$rs = $this->ci->db->get('test_groups as a');
    	return $rs->num_rows() > 0 ? $rs->result_array() : FALSE;
    }

    public function get_transaction($applicant_id, $test_type_id){
		$this->ci->db->select('id');
		$this->ci->db->where('applicant_id', $applicant_id);
		$this->ci->db->where('test_type_id', $test_type_id);
		$rs = $this->ci->db->get('test_transaction');
		return $rs->num_rows() > 0 ? $rs->row_array() : FALSE;
    }

}