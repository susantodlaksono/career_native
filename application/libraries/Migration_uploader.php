<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

require_once 'Excel_reader.php';

class Migration_uploader extends Excel_reader{

   public function __construct() {
      $this->ci = & get_instance();
      $this->ci->load->library('upload');
   }

   public function upload($_filename = NULL, $_input_name = 'file_upload', $_allowed_types = 'xlsx', $_dir = './migration/', $_max_size_file = 10000){
		$config['upload_path'] =  $_dir;
      $config['allowed_types'] = $_allowed_types;
      $config['max_size'] = $_max_size_file;
      $config['file_name'] = $_filename;

      $this->ci->upload->initialize($config);
      if ($this->ci->upload->do_upload($_input_name)) {
   		$response['status'] = TRUE;
   	}else{
   		$response['status'] = FALSE;
		 $response['msg'] = $this->ci->upload->display_errors();
      }
   	return $response;
   }

   public function question($user_id, $params, $_filepath = NULL, $_filename = NULL){
      ini_set('memory_limit', '256M');
      if($_filename && $_filepath){
         $excel_reader = $this->read(NULL, $_filepath, $_filename.'.xlsx');
         $phpexcel = $excel_reader['result'];
         $data = array();
         $i = 0;
         $row = 1;
         $phpexcel->setActiveSheetIndex(0);   
         $multiplechoice = $phpexcel->getActiveSheet()->toArray(null, true, true, true);
         foreach ($multiplechoice as $v) {
            if ($row != 1) {
               if(isset($v['A']) && $v['A'] != ''){
                  $data[$i]['question'] = $v['A'];
                  $answers = array();
                  if($v['B'] !== ''){
                     array_push($answers, array('text' => $v['B'], 'correct' => $v['G'] != '' && $v['G'] == 'A' || $v['G'] == 'a' ? TRUE : FALSE));
                  }
                  if($v['C'] !== ''){
                     array_push($answers, array('text' => $v['C'], 'correct' => $v['G'] != '' && $v['G'] == 'B' ||$v['G'] == 'b' ? TRUE : FALSE));
                  }
                  if($v['D'] !== ''){
                     array_push($answers, array('text' => $v['D'], 'correct' => $v['G'] != '' && $v['G'] == 'C' || $v['G'] == 'c' ? TRUE : FALSE));
                  }
                  if($v['E'] !== ''){
                     array_push($answers, array('text' => $v['E'], 'correct' => $v['G'] != '' && $v['G'] == 'D' || $v['G'] == 'd' ? TRUE : FALSE));
                  }
                  if($v['F'] !== ''){
                     array_push($answers, array('text' => $v['F'], 'correct' => $v['G'] != '' && $v['G'] == 'E' || $v['G'] == 'e' ? TRUE : FALSE));
                  }
                  $data[$i]['answers'] = $answers;
               }
            }
            $i++;
            $row++;
         }
         if($data){
            $countsuccess = 0;
            $counterror = 0;
            foreach ($data as $v) {
               try {
                  $tmpquestion = array(
                     'question_text' => $v['question'] != '' ? utf8_encode($v['question']) : NULL,
                     'test_type_id' => $params['type_test'],
                     'question_type_id' => 1
                  );
                  $this->ci->db->insert('question', $tmpquestion);  
                  $qid = $this->ci->db->insert_id();

                  foreach ($v['answers'] as $vv) {
                     $tmpanswers = array(
                        'question_id' => $qid,
                        'question_answers' => $vv['text'],
                        'correct_answers' => $vv['correct'] ? $vv['correct'] : NULL
                    );
                    $this->ci->db->insert('question_answers', $tmpanswers);
                  }
                  $countsuccess++;
               } catch (Exception $e) {
                  $counterror++;
               }
            }
            $response['status'] = TRUE;
            $response['msg'] = 'Soal ditambahkan ('.$countsuccess.' data berhasil ditambahkan, '.$counterror.' gagal ditambahkan)';
         }else{
            $response['status'] = FALSE;
            $response['msg'] = 'Row data not found';
         }
      }else{
         $response['status'] = FALSE;
         $response['msg'] = 'Reader not definition';
      }
      // if($_filepath){
      //    $this->clear_dir($_filepath);
      // }
      return $response;
   }

   public function q_upload_file($user_id, $params, $_filepath = NULL, $_filename = NULL){
      ini_set('memory_limit', '256M');
      if($_filename && $_filepath){
         $excel_reader = $this->read(NULL, $_filepath, $_filename.'.xlsx');
         $phpexcel = $excel_reader['result'];
         $data = array();
         $i = 0;
         $row = 1;
         $phpexcel->setActiveSheetIndex(1);
         $uploadfilequestion = $phpexcel->getActiveSheet()->toArray(null, true, true, true);
         foreach ($uploadfilequestion as $v) {
            if ($row != 1) {
               if(isset($v['A']) && $v['A'] != ''){
                  $data[$i]['question'] = $v['A'];
               }
            }
            $i++;
            $row++;
         }
         if($data){
            $countsuccess = 0;
            $counterror = 0;
            foreach ($data as $v) {
               try {
                  $tmpquestion = array(
                     'question_text' => $v['question'] != '' ? utf8_encode($v['question']) : NULL,
                     'test_type_id' => $params['type_test'],
                     'question_type_id' => 2
                  );
                  $this->ci->db->insert('question', $tmpquestion);  
               } catch (Exception $e) {
                  $counterror++;
               }
            }
            $response['status'] = TRUE;
            $response['msg'] = 'Soal ditambahkan ('.$countsuccess.' data berhasil ditambahkan, '.$counterror.' gagal ditambahkan)';
         }else{
            $response['status'] = FALSE;
            $response['msg'] = 'Row data not found';
         }
      }else{
         $response['status'] = FALSE;
         $response['msg'] = 'Reader not definition';
      }
      // if($_filepath){
      //    $this->clear_dir($_filepath);
      // }
      return $response;
   }

   public function q_description($user_id, $params, $_filepath = NULL, $_filename = NULL){
      ini_set('memory_limit', '256M');
      if($_filename && $_filepath){
         $excel_reader = $this->read(NULL, $_filepath, $_filename.'.xlsx');
         $phpexcel = $excel_reader['result'];
         $data = array();
         $i = 0;
         $row = 1;
         $phpexcel->setActiveSheetIndex(3);
         $uploadfilequestion = $phpexcel->getActiveSheet()->toArray(null, true, true, true);
         foreach ($uploadfilequestion as $v) {
            if ($row != 1) {
               if(isset($v['A']) && $v['A'] != ''){
                  $data[$i]['question'] = $v['A'];
               }
            }
            $i++;
            $row++;
         }
         if($data){
            $countsuccess = 0;
            $counterror = 0;
            foreach ($data as $v) {
               try {
                  $tmpquestion = array(
                     'question_text' => $v['question'] != '' ? utf8_encode($v['question']) : NULL,
                     'test_type_id' => $params['type_test'],
                     'question_type_id' => 3
                  );
                  $this->ci->db->insert('question', $tmpquestion);  
               } catch (Exception $e) {
                  $counterror++;
               }
            }
            $response['status'] = TRUE;
            $response['msg'] = 'Soal ditambahkan ('.$countsuccess.' data berhasil ditambahkan, '.$counterror.' gagal ditambahkan)';
         }else{
            $response['status'] = FALSE;
            $response['msg'] = 'Row data not found';
         }
      }else{
         $response['status'] = FALSE;
         $response['msg'] = 'Reader not definition';
      }
      // if($_filepath){
      //    $this->clear_dir($_filepath);
      // }
      return $response;
   }

   public function q_multi_answers($user_id, $params, $_filepath = NULL, $_filename = NULL){
      ini_set('memory_limit', '256M');
      if($_filename && $_filepath){
         $excel_reader = $this->read(NULL, $_filepath, $_filename.'.xlsx');
         $phpexcel = $excel_reader['result'];
         $data = array();
         $i = 0;
         $row = 1;
         $phpexcel->setActiveSheetIndex(2);   
         $multiplechoice = $phpexcel->getActiveSheet()->toArray(null, true, true, true);
         foreach ($multiplechoice as $v) {
            if ($row != 1) {
               if(isset($v['A']) && $v['A'] != ''){
                  $g_column = $v['G'];
                  $correct_list = explode(',', $g_column);

                  $data[$i]['question'] = $v['A'];
                  $answers = array();
                  if($v['B'] !== ''){
                     array_push($answers, array('text' => $v['B'], 'correct' => $v['G'] != '' && in_array('A', $correct_list) || in_array('a', $correct_list) ? TRUE : FALSE));
                  }
                  if($v['C'] !== ''){
                     array_push($answers, array('text' => $v['C'], 'correct' => $v['G'] != '' && in_array('B', $correct_list) || in_array('b', $correct_list) ? TRUE : FALSE));
                  }
                  if($v['D'] !== ''){
                     array_push($answers, array('text' => $v['D'], 'correct' => $v['G'] != '' && in_array('C', $correct_list) || in_array('c', $correct_list) ? TRUE : FALSE));
                  }
                  if($v['E'] !== ''){
                     array_push($answers, array('text' => $v['E'], 'correct' => $v['G'] != '' && in_array('D', $correct_list) || in_array('d', $correct_list) ? TRUE : FALSE));
                  }
                  if($v['F'] !== ''){
                     array_push($answers, array('text' => $v['F'], 'correct' => $v['G'] != '' && in_array('e', $correct_list) || in_array('e', $correct_list) ? TRUE : FALSE));
                  }
                  $data[$i]['answers'] = $answers;
               }
            }
            $i++;
            $row++;
         }
         if($data){
            $countsuccess = 0;
            $counterror = 0;
            foreach ($data as $v) {
               try {
                  $tmpquestion = array(
                     'question_text' => $v['question'] != '' ? utf8_encode($v['question']) : NULL,
                     'test_type_id' => $params['type_test'],
                     'question_type_id' => 4
                  );
                  $this->ci->db->insert('question', $tmpquestion);  
                  $qid = $this->ci->db->insert_id();

                  foreach ($v['answers'] as $vv) {
                     $tmpanswers = array(
                        'question_id' => $qid,
                        'question_answers' => $vv['text'],
                        'correct_answers' => $vv['correct'] ? $vv['correct'] : NULL
                    );
                    $this->ci->db->insert('question_answers', $tmpanswers);
                  }
                  $countsuccess++;
               } catch (Exception $e) {
                  $counterror++;
               }
            }
            $response['status'] = TRUE;
            $response['msg'] = 'Soal ditambahkan ('.$countsuccess.' data berhasil ditambahkan, '.$counterror.' gagal ditambahkan)';
         }else{
            $response['status'] = FALSE;
            $response['msg'] = 'Row data not found';
         }
      }else{
         $response['status'] = FALSE;
         $response['msg'] = 'Reader not definition';
      }
      // if($_filepath){
      //    $this->clear_dir($_filepath);
      // }
      return $response;
   }

   public function clear_dir($dir){
      $files = glob(''.$dir.'*'); 
      foreach($files as $file){
         if(is_file($file))
         unlink($file);
      }
   }
}