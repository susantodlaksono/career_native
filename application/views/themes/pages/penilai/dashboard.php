<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>
                        <th>Nama</th>
                        <th>Pendidikan</th>
                        <th>Test</th>
                        <th>Tgl</th>
                        <th>Status</th>
                        <th></th>
                    </thead>
                    <tbody class="data-evaluator">
                        <?php
                        foreach ($pelamar as $k => $v) {
                            echo '<tr>';
                            echo '<td>'.$v['name'].'</td>';
                            echo '<td>'.$v['education_degree'].'</td>';
                            echo '<td>'.$v['test_type_name'].'</td>';
                            echo '<td>'.$v['time_start'].'</td>';
                            if($v['checked_transaction'] == 0){
                                echo '<td><span class="label label-default">Belum diperiksa</span></td>';
                            }
                            if($v['checked_transaction'] == 1){
                                echo '<td><span class="label label-success">Telah Diperiksa</span></td>';
                            }
                            echo '<td>';
                                echo '<div class="btn-group">';
                                    $qtype = $this->db->select('question_type_id')->where('test_type_id', $v['test_type_id'])->group_by('question_type_id')->get('question')->result_array();
                                    foreach ($qtype as $vv) {
                                        if($vv['question_type_id'] == 2){
                                            echo '<a class="btn btn-default btn-xs" href="'.site_url().'main/detailTestPelamar/'.$v['test_type_id'].'/'.$v['id'].'/'.$vv['question_type_id'].'">Detail Upload File</a>'; 
                                        }
                                        if($vv['question_type_id'] == 3){
                                            echo '<a class="btn btn-default btn-xs" href="'.site_url().'main/detailTestPelamar/'.$v['test_type_id'].'/'.$v['id'].'/'.$vv['question_type_id'].'">Detail Deskripsi</a>'; 
                                        }
                                    }
                                    if($v['checked_transaction'] == 0){
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'main/rubahStatusPelamar/'.$v['id'].'/1">Tandai telah diperiksa</button>';
                                    }
                                    if($v['checked_transaction'] == 1){
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'main/rubahStatusPelamar/'.$v['id'].'/0">Tandai belum diperiksa</button>';
                                    }
                                echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable({
            "order": []
        });

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>