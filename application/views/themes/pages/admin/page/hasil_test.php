<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>
                        <th>Nama</th>
                        <th>Lowongan</th>
                        <th>Kontak</th>
                        <th width="250">email</th>
                        <th>Tgl Register</th>
                        <th>Lolos/Tidak</th>
                        <th class="text-center"></th>
                    </thead>
                    <tbody class="data-pelamar">
                        <?php
                        if($hasil_test){
                            foreach ($hasil_test as $k => $v) {
                                echo '<tr>';
                                echo '<td>'.$v['name'].'</td>';
                                echo '<td>'.$v['vacancy_name'].'</td>';
                                echo '<td>'.$v['contact_number'].'</td>';
                                echo '<td>'.$v['email'].'</td>';
                                echo '<td>'.$v['created'].'</td>';
                                if($v['status']){
                                    if($v['status'] == 1){
                                        echo '<td><span class="label label-danger bold">Tidak Lolos</span></td>';
                                    }
                                    if($v['status'] == 2){
                                        echo '<td><span class="label label-success bold">Lolos</span></td>';
                                    }
                                }else{
                                    echo '<td><span class="label label-default">Belum Dinilai</span></td>';
                                }
                                echo '<td>';
                                    echo '<div class="btn-group">';
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'hasil_test/detail/'.$v['id'].'/'.$v['vacancy_id'].'">Detail</a>';
                                    echo '</div>';
                                echo '</td>';
                                echo '</tr>';
                            }
                        }else{
                            echo '<tr><td colspan="10">Tidak ditemukan</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>