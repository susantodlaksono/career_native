<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-8">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
         		<form method="post" action="<?php echo site_url()?>manage_test/insert">
         			<div class="form-group">
                      	<label class="text-info">Nama Test <span class="text-danger">*</span></label>
                      	<input type="text" class="form-control input-sm" name="name" value="" />
                   </div>
                   <div class="form-group">
                      	<label>Jawaban Lengkap <span class="text-danger">*</span></label>
                      	<select class="form-control input-sm" name="mode_test" />
                         	<option value="1">Ya</option>
                         	<option value="2">Tidak</option>
                      	</select>
                   </div>
                   <div class="form-group">
                      	<label class="text-info">Total Pertanyaan <span class="text-danger">*</span></label>
                      	<input type="text" class="form-control input-sm" name="total_question" value="" />
                   </div>
                   <div class="form-group">
                  		<label>Waktu <span class="text-danger">*</span></label>
                  		<div class="input-group">
                  			<input type="number" class="form-control" name="times">
                  			<span class="input-group-addon default">
                                Menit
                            </span>
              			</div>
                	</div>
                	<div class="form-group">
                      	<label>Status <span class="text-danger">*</span></label>
                      	<select class="form-control input-sm" name="status" />
                         	<option value="1">Active</option>
                         	<option value="">Inactive</option>
                      	</select>
                   </div>
                	<div class="form-group">
						<label>Instruksi Test</label>
						<textarea class="form-control" name="guidetest"></textarea>
					</div>
                   <a href="<?php echo site_url('manage_test') ?>" class="btn btn-default btn-block">Kembali</a>
                   <button type="submit" class="btn btn-info btn-block">Simpan</button>
         		</form>
      		</div>
   		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>

<script type="text/javascript">
	$(function () {
		tinymce.init({
	        selector: 'textarea',
	        menubar: false,
	        style: "border:5px solid black;",
	        mobile: { theme: 'mobile' }
	    });
	});
</script>