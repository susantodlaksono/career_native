<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-6">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
         		<form method="post" action="<?php echo site_url()?>lowongan/insert">
         			<div class="form-group">
                      	<label class="text-info">Nama Lowongan <span class="text-danger">*</span></label>
                      	<input type="text" class="form-control input-sm" name="name" value="" />
                   </div>
                	<div class="form-group">
                      	<label>Status <span class="text-danger">*</span></label>
                      	<select class="form-control input-sm" name="status" />
                         	<option value="1">Active</option>
                         	<option value="">Inactive</option>
                      	</select>
                   </div>
                   <a href="<?php echo site_url('lowongan') ?>" class="btn btn-default btn-block">Kembali</a>
                   <button type="submit" class="btn btn-info btn-block">Simpan</button>
         		</form>
      		</div>
   		</div>
	</div>
</div>