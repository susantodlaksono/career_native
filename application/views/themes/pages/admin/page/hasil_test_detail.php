<style type="text/css">
	.table.table-condensed thead tr th, .table.table-condensed tbody tr td, .table.table-condensed tbody tr td *:not(.dropdown-default){
		overflow: unset;
	}
</style>
<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="<?php echo site_url() ?>hasil_test" class="btn btn-default btn-sm">
                Kembali
            </a>
        </div>
    </div>
</div>
 <?php
if($this->session->userdata('notif') != ''){
    echo '<div class="alert alert-info" role="alert">';
        echo '<button class="close" data-dismiss="alert"></button>';
        echo $this->session->userdata('notif');
    echo '</div>';
    $this->session->sess_destroy();
}
?>
<div class="row">
    <div class="col-md-4">
    	<div class="panel panel-default" style="margin-bottom: 0;">
            <div class="panel-heading">
            	<div class="panel-title">Profil Pelamar</div>
          	</div>
            <div class="panel-body">
                <table class="table table-condensed">
                    <tbody>
                        <tr>
                            <td style="border-top: 0" width="170" class="bold">Nama</td>
                            <td style="border-top: 0"><?php echo $pelamar['name'] ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold">Email</td>
                            <td><?php echo $pelamar['email'] ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold">No. Handphone</td>
                            <td><?php echo $pelamar['contact_number'] ?></td>
                        </tr>
                        <tr>
                            <td width="190" class="bold">Tingkat Pendidikan</td>
                            <td><?php echo $pelamar['education_degree'] ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold">Universitas/Sekolah</td>
                            <td><?php echo $pelamar['university'] ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold">Bidang</td>
                            <td><?php echo $pelamar['school_majors'] ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold">Birth Date</td>
                            <td><?php echo $pelamar['birth_date'] ? date('d M Y', strtotime($pelamar['birth_date'])) : '' ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold">Lowongan</td>
                            <td style=""><?php echo $pelamar['vacancy_name'] ?></td>
                        </tr>
                        <tr>
                            <td width="150" class="bold" style="">Level</td>
                            <td style=""><?php echo $pelamar['level'] == 1 ? 'Fresh Graduate' : 'Expert' ?></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bold" style="">Status lolos :</td>
                        </tr>
                        <tr>
                            <form method="post" action="<?php echo site_url()?>hasil_test/status_lolos">
                            <input type="hidden" name="applicant_id" value="<?php echo $pelamar['id'] ?>">
                            <input type="hidden" name="id" value="<?php echo $id ?>">
                            <input type="hidden" name="vacancyid" value="<?php echo $vacancyid ?>">
                            <td width="150" class="bold" style="">
                                <select class="form-control input-sm" name="status">
                                    <option value="" <?php echo $pelamar['status'] ? '' : 'selected=""'?>></option>
                                    <option value="2" <?php echo $pelamar['status'] == 2 ?'selected=""' : ''?>>Ya</option>
                                    <option value="1" <?php echo $pelamar['status'] == 1 ? 'selected=""' : ''?>>Tidak</option>
                                </select>
                            </td>
                            <td style=""><button type="submit" class="btn btn-primary">Submit</button></td>
                            </form>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
	<div class="col-md-8">
    	<div class="panel panel-default" style="margin-bottom: 0;">
            <div class="panel-heading">
            	<div class="panel-title">Hasil Test</div>
          	</div>
            <div class="panel-body" style="margin-top: 10px;">
            	<div class="row">
            	<div class="col-md-12">
            	<?php
            	if($hasil_test){
            		foreach ($hasil_test as $k => $v) {
            			echo '<div class="panel panel-success">';
				            echo '<div class="panel-heading" style="padding:10px;height:auto;min-height:0">';
				            	echo '<div class="panel-title text-center">'.$v['name'].'</div>';
				          	echo '</div>';
				            echo '<div class="panel-body">';
				                echo '<div class="row">';
				                	if($v['result']){
					                	foreach ($v['result'] as $vv) {
					                		echo '<div class="col-md-3 text-center">';
                                                echo '<h1 class="bold no-margin total-applicant text-success">'.($vv['assessment'] ? $vv['assessment'] : 0).'</h1>';
						                        echo '<h6 class="bold" style="margin-top:7px">'.$vv['question_type_name'].'</h6>';
						                        echo '<h6 class="no-margin text-muted">Total Pertanyaan : '.$vv['total_question'].'</h6>';
						                        echo '<h6 class="no-margin text-muted">';
                                                    if(in_array($vv['question_type_id'], array(2,3))){
                                                        echo 'Nilai : '.($vv['result'] ? $vv['result'] : 0).'';
                                                        echo '&nbsp;<a href="'.site_url().'hasil_test/penilaian/'.$k.'/'.$vv['test_transaction_id'].'/'.$vv['question_type_id'].'/'.$id.'/'.$vacancyid.'" style="margin-top:15px;margin-bottom:5px;"><i class="fa fa-edit"></i></a>';
                                                    }else{
                                                        if($vv['question_type_id'] == 5){
                                                            echo '<a href="" data-rs="'.$vv['assessment'].'" class="detail-psikotest">Detail Kepribadian</a>';
                                                        }else{
                                                            echo 'Jumlah benar : '.($vv['result'] ? $vv['result'] : 0).'';  
                                                        }
                                                    }
                                                echo '</h6>';
						                    echo '</div>';
				                		}
			                		}else{
			                			echo '<h6 class="text-center text-danger">Hasil tidak ditemukan</h6>';
			                		}
				                echo '</div>';
				            echo '</div>';
				        echo '</div>';
            		}
            	}
            	?>
        		</div>
        		</div>
        	</div>
    	</div>
	</div>
</div>

<div class="modal fade" id="modal-detail-psikotest">
	<div class="modal-dialog modal-lg">
  		<div class="modal-content-wrapper">
    		<div class="modal-content">
    			<div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    	<i class="pg-close fs-14"></i>
                    </button>
                    <h5 class="bold modal-title">Detail Kepribadian</h5>
                  </div>
	          	<div class="modal-body" style="margin-top: 20px;">
          		</div>
          	</div>
      	</div>
  	</div>
</div>

<script type="text/javascript">
    

    $(function(){

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();

    	$(this).on('click', '.detail-psikotest', function (e){
	  		var code = $(this).data('rs');
            ajaxManager.addReq({
                url: site_url + 'hasil_test/detail_psikotest',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    code: code
                },
                beforeSend: function () {
                   
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    t = '';
                    $.each(r.description, function(k,v){
                        t += '<li>'+v+'</li>';
                    });
                    $('#modal-detail-psikotest .modal-body').html(t);
                    $('#modal-detail-psikotest').modal('show');
                }
            });
	  		e.preventDefault();
	  	});

    });

</script>