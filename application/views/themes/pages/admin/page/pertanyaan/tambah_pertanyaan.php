<style type="text/css">
    .radio label, .checkbox label{
        margin-right: 0;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-12">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <form method="post" action="<?php echo site_url()?>pertanyaan/insert" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pertanyaan untuk test <span class="text-danger">*</span></label>
                                <select class="form-control" name="type_test" />
                                    <?php
                                    if($test){
                                        foreach ($test as $v) {
                                            echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tambahkan gambar untuk pertanyaan</label>
                                <input type="file" name="question_image" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tambahkan file untuk pertanyaan</label>
                                <input type="file" name="question_file" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Jenis Jawaban</label>
                                <div class="radio radio-success">
                                  <input type="radio" value="1" name="type_answers" id="radio1"><label for="radio1">Multiple Choice</label>
                                  <input type="radio" value="2" name="type_answers" id="radio2"><label for="radio2">File Upload</label>
                                  <input type="radio" value="3" name="type_answers" id="radio3"><label for="radio3">Jawaban Deskripsi</label>
                                  <input type="radio" value="4" name="type_answers" id="radio4"><label for="radio4">Jawaban Ganda</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Deskripsi Pertanyaan</label>
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="section-multiple-choice" style="display: none;">
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="" class="add-answers-multiple-choice">
                                    <p><i class="fa fa-plus"></i> Tambah Jawaban Lainnya</p>
                                </a>
                                <div class="multiple-choice">
                                    <div class="input-group" style="margin-bottom:10px;">
                                        <div class="input-group-btn">
                                            <label class="btn btn-default btn-sm" style="height:35px;">
                                                <div class="radio radio-danger" style="margin:0;padding:0">
                                                    <input name="correct" value="0" type="radio" id="cr-0" class="choose-correct-mchoice">
                                                    <label for="cr-0" style="margin:0;padding:0">Jawaban Benar</label>
                                                </div>
                                            </label>
                                        </div>
                                        <input type="text" class="form-control" name="ans_mchoice[0][result]" placeholder="Jawaban...">
                                        <input type="hidden" name="ans_mchoice[0][correct]" class="ans-mchoice">
                                    </div>
                                    <div class="input-group" style="margin-bottom:10px;">
                                        <div class="input-group-btn">
                                            <label class="btn btn-default btn-sm" style="height:35px;">
                                                <div class="radio radio-danger" style="margin:0;padding:0">
                                                    <input name="correct" value="1" type="radio" id="cr-1" class="choose-correct-mchoice">
                                                    <label for="cr-1" style="margin:0;padding:0">Jawaban Benar</label>
                                                </div>
                                            </label>
                                        </div>
                                        <input type="text" class="form-control" name="ans_mchoice[1][result]" placeholder="Jawaban...">
                                        <input type="hidden" name="ans_mchoice[1][correct]" class="ans-mchoice">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="section-multiple-answers" style="display: none;">
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="" class="add-answers-multiple-answers">
                                    <p><i class="fa fa-plus"></i> Tambah Jawaban Lainnya</p>
                                </a>
                                <div class="multiple-answers">
                                    <div class="input-group" style="margin-bottom:10px;">
                                        <div class="input-group-btn">
                                            <label class="btn btn-default btn-sm" style="height:35px;">
                                                <input type="checkbox" name="correct[]" value="0" class="choose-correct-manswers"> 
                                                <label style="font-weight: normal;font-size:13px;">Jawaban Benar</label>
                                            </label>
                                        </div>
                                        <input type="text" class="form-control" name="ans_manswers[0][result]" placeholder="Jawaban...">
                                        <input type="hidden" name="ans_manswers[0][correct]" class="ans-manswers">
                                    </div>
                                    <div class="input-group" style="margin-bottom:10px;">
                                        <div class="input-group-btn">
                                            <label class="btn btn-default btn-sm" style="height:35px;">
                                                <input type="checkbox" name="correct[]" value="1" class="choose-correct-manswers"> 
                                                <label style="font-weight: normal;font-size:13px;">Jawaban Benar</label>
                                            </label>
                                        </div>
                                        <input type="text" class="form-control" name="ans_manswers[1][result]" placeholder="Jawaban...">
                                        <input type="hidden" name="ans_manswers[1][correct]" class="ans-manswers">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo site_url('pertanyaan') ?>" class="btn btn-default btn-block">Kembali</a>
                    <button type="submit" class="btn btn-info btn-block">Simpan</button>
                </form>
      		</div>
   		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>

<script type="text/javascript">
    $(function () {

        key_correct_answers = 1;

        tinymce.init({
            selector: 'textarea',
            menubar: false,
            height: '180px',
            style: "border:5px solid black;",
            mobile: { theme: 'mobile' }
        });

        $('input:radio[name="type_answers"]').change(
            function(){
                if ($(this).is(':checked') && $(this).val() == 1) {
                    $('#section-multiple-choice').show();
                    $('#section-multiple-answers').hide();
                }else if ($(this).is(':checked') && $(this).val() == 4) {
                    $('#section-multiple-answers').show();
                    $('#section-multiple-choice').hide();
                }else{
                    $('#section-multiple-choice').hide();
                    $('#section-multiple-answers').hide();
                }
            }
        );

        $(this).on('change', '.choose-correct-mchoice', function (e) {
            var key = $(this).val();
            $('.ans-mchoice').val('');
            $('input[name="ans_mchoice['+key+'][correct]"]').val(1);
        });

        $(this).on('change', '.choose-correct-manswers', function (e) {
            var key = $(this).val();
            if(this.checked) {
                $('input[name="ans_manswers['+key+'][correct]"]').val(1);
            }else{
                $('input[name="ans_manswers['+key+'][correct]"]').val('');
            }
        });

        $('.add-answers-multiple-choice').on('click', function (e) {
            var mode = $(this).data('mode');
            key_correct_answers += 1;
            ans = '';
            ans += '<div class="input-group" style="margin-bottom:10px;">';
                ans += '<div class="input-group-btn">';
                    ans += '<label class="btn btn-default btn-sm" style="height:35px;">';
                        ans += '<div class="radio radio-danger" style="margin:0;padding:0">'
                            ans += '<input name="correct" value="'+key_correct_answers+'" type="radio" class="choose-correct-mchoice">';
                            ans += '<label for="cr-'+key_correct_answers+'" style="margin:0;padding:0">Jawaban Benar</label>';
                        ans += '</div>';
                    ans += '</label>';
                ans += '</div>';
                ans += '<input type="text" class="form-control" name="ans_mchoice['+key_correct_answers+'][result]" placeholder="Jawaban...">';
                ans += '<input type="hidden" name="ans_mchoice['+key_correct_answers+'][correct]" class="ans-mchoice">';
                ans += '<div class="input-group-btn">';
                    ans += '<button type="button" class="btn btn-danger btn-remove-answers"><i class="fa fa-trash"></i></button>';
                ans += '</div>';
            ans += '</div>';
            $('.multiple-choice').append(ans);
            e.preventDefault();

            $('.btn-remove-answers').on('click', function (e) {
                $(this).parents('.input-group').remove();
                e.preventDefault();
            });

            $(this).on('change', '.choose-correct-mchoice', function (e) {
                var key = $(this).val();
                $('.ans-mchoice').val('');
                $('input[name="ans_mchoice['+key+'][correct]"]').val(1);
            });

        });

        $('.add-answers-multiple-answers').on('click', function (e) {
            var mode = $(this).data('mode');
            key_correct_answers += 1;
            ans = '';
            ans += '<div class="input-group" style="margin-bottom:10px;">';
                ans += '<div class="input-group-btn">';
                    ans += '<label class="btn btn-default btn-sm" style="height:35px;">';
                        ans += '<input type="checkbox" name="correct['+key_correct_answers+']" value="'+key_correct_answers+'" class="choose-correct-manswers">&nbsp;'; 
                        ans += '<label style="font-weight: normal;font-size:13px;">Jawaban Benar</label>';
                    ans += '</label>';
                ans += '</div>';
                ans += '<input type="text" class="form-control" name="ans_manswers['+key_correct_answers+'][result]" placeholder="Jawaban...">';
                ans += '<input type="hidden" name="ans_manswers['+key_correct_answers+'][correct]" class="ans-manswers">';
                ans += '<div class="input-group-btn">';
                    ans += '<button type="button" class="btn btn-danger btn-remove-answers"><i class="fa fa-trash"></i></button>';
                ans += '</div>';
            ans += '</div>';
            $('.multiple-answers').append(ans);
            e.preventDefault();

            $('.btn-remove-answers').on('click', function (e) {
                $(this).parents('.input-group').remove();
                e.preventDefault();
            });

            $(this).on('change', '.choose-correct-manswers', function (e) {
                var key = $(this).val();
                if(this.checked) {
                    $('input[name="ans_manswers['+key+'][correct]"]').val(1);
                }else{
                    $('input[name="ans_manswers['+key+'][correct]"]').val('');
                }
            });

        });
    });
</script>