<style type="text/css">
    .radio label, .checkbox label{
        margin-right: 0;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>

<div class="row">
 	<div class="col-md-6">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <form method="post" action="<?php echo site_url()?>pertanyaan/export" enctype="multipart/form-data">
                	<div class="form-group">
                        <a href="<?php echo base_url() ?>import_format/formatsoal.xlsx"><i class="fa fa-download"></i> Format Soal</a>
                    </div>
                	<div class="form-group">
                        <label>Pertanyaan untuk test</label>
                        <select class="form-control" name="type_test" />
                            <?php
                            if($test){
                                foreach ($test as $v) {
                                    echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                    	<label>Upload Format</label>
                  		<input type="file" name="file_upload" class="form-control">
	               	</div>
                    <a href="<?php echo site_url('pertanyaan') ?>" class="btn btn-default btn-block">Kembali</a>
                    <button type="submit" class="btn btn-info btn-block">Import</button>
            	</form>
        	</div>
    	</div>
	</div>
</div>