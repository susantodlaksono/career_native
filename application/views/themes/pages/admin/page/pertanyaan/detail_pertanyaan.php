<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.css">
<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="<?php echo site_url() ?>pertanyaan" class="btn btn-default btn-sm">
                kembali
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="table-pertanyaan">
                    <thead>
                        <th>Jenis</th>
                        <th width="600">Pertanyaan</th>
                        <th>File</th>
                        <th>Gambar</th>
                        <th>Jawaban Benar</th>
                        <th></th>
                    </thead>
                    <tbody>
                    <?php 
                    if($pertanyaan){
                        foreach ($pertanyaan as $v) {
                            switch ($v['question_type_id']) {
                                case '1': echo '<td>Multiple Choice</td>';break;
                                case '2': echo '<td>Upload File</td>';break;
                                case '3': echo '<td>Jawaban Deskripsi</td>';break;
                                case '4': echo '<td>Jawaban Ganda</td>';break;
                            }
                            if($v['question_text']){
                                echo '<td>'.strip_tags($v['question_text']).'</td>';
                            }else{
                                echo '<td class="text-danger">None</td>';   
                            }
                            if($v['file']){
                                echo '<td>';
                                echo '<a href="'.base_url().'files/pertanyaan_file/'.$v['file'].'">'.$v['temp_name_file'].'</a>';
                                echo '</td>';
                            }else{
                                echo '<td class="text-danger">None</td>';    
                            }
                            if($v['image']){
                                echo '<td>';
                                echo '<a data-fancybox="gallery" href="'.base_url().'files/pertanyaan_gambar/'.$v['image'].'">';
                                    echo '<img src="'.base_url().'files/pertanyaan_gambar/'.$v['image'].'" width="50" height="50">';
                                echo '</a>';
                                echo '</td>';
                            }else{
                                echo '<td class="text-danger">None</td>';
                            }
                            if($v['jawaban_benar']){
                                echo '<td>'.$v['jawaban_benar'].'</td>';
                            }else{
                                echo '<td class="text-danger">None</td>';
                            }
                            echo '<td>';
                                echo '<div class="btn-group">';
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'pertanyaan/edit/'.$v['id'].'/'.$test_type_id.'">Edit</a>'; 
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'pertanyaan/hapus/'.$v['id'].'/'.$test_type_id.'">Delete</a>';
                                echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    }else{
                        echo '<tr><td colspan="5" class="text-center">Data tidak ditemukan<td></tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>  
</div>
<script src="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.js"></script>

<script type="text/javascript">
    

    $(function(){

        $('#table-pertanyaan').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>