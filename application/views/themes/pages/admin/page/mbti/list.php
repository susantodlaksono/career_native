<?php
function mapPersonality($data){
    switch ($data) {
        case 'I': return 'Introvert'; break;
        case 'S': return 'Sensing'; break;
        case 'T': return 'Thinking'; break;
        case 'J': return 'Judging'; break;
        case 'E': return 'Ekstrovert'; break;
        case 'N': return 'Intuition'; break;
        case 'F': return 'Feeling'; break;
        case 'P': return 'Perceiving'; break;
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>                        
                        <th>Pernyataan 1</th>
                        <th>Pernyataan 2</th>                        
                        <th></th>
                    </thead>
                    <tbody>
                    <?php 
                    if($result){
                        foreach ($result as $k => $v) {
                            echo '<tr>';
                            echo '<td><b>['.$v[0]['question_answers_alias'].']</b>&nbsp;'.$v[0]['question_answers'].'&nbsp;<b>('.mapPersonality($v[1]['question_answers_alias']).')</b></td>';
                            echo '<td><b>['.$v[1]['question_answers_alias'].']</b>&nbsp;'.$v[1]['question_answers'].'&nbsp;<b>('.mapPersonality($v[1]['question_answers_alias']).')</b></td>';
                            echo '<td>';
                                echo '<div class="btn-group">';
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'mbti/edit/'.$k.'">Edit</a>'; 
                                echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                    }else{
                        echo '<tr><td colspan="5" class="text-center">Data tidak ditemukan<td></tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>  
</div>
<script src="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.js"></script>

<script type="text/javascript">
    

    $(function(){

         $('#dt-table').DataTable({
            "order": []
        });

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>