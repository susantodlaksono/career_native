<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>

<div class="row">
 	<div class="col-md-12">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <form method="post" action="<?php echo site_url()?>mbti/update">
                	<?php
                	foreach ($result as $v) {
                		echo '<div class="row">';
	                		echo '<div class="col-md-6">';
	                			echo '<div class="form-group">';
	                				echo '<label>Pernyataan</label>';
	                				echo '<input type="text" name="data['.$v['id'].'][question_answers]" class="form-control input-sm" value="'.$v['question_answers'].'">';
	            				echo '</div>';
            				echo '</div>';
	            			echo '<div class="col-md-6">';
	                			echo '<div class="form-group">';
	                				echo '<label>Type Personality</label>';
	                				echo '<select name="data['.$v['id'].'][question_answers_alias]" class="form-control">';
	                				echo '<option value="I" '.($v['question_answers_alias'] == 'I' ? 'selected' : '').'>Introvert</option>';
	                				echo '<option value="S" '.($v['question_answers_alias'] == 'S' ? 'selected' : '').'>Sensing</option>';
	                				echo '<option value="T" '.($v['question_answers_alias'] == 'T' ? 'selected' : '').'>Thinking</option>';
	                				echo '<option value="J" '.($v['question_answers_alias'] == 'J' ? 'selected' : '').'>Judging</option>';
	                				echo '<option value="E" '.($v['question_answers_alias'] == 'E' ? 'selected' : '').'>Ekstrovert</option>';
	                				echo '<option value="N" '.($v['question_answers_alias'] == 'N' ? 'selected' : '').'>Intuition</option>';
	                				echo '<option value="F" '.($v['question_answers_alias'] == 'F' ? 'selected' : '').'>Feeling</option>';
	                				echo '<option value="P" '.($v['question_answers_alias'] == 'P' ? 'selected' : '').'>Perceiving</option>';
	                				echo '</select>';	
	                			echo '</div>';	
	                		echo '</div>';	
            			echo '</div>';
                	}
                	?>
                	<a href="<?php echo site_url('mbti') ?>" class="btn btn-default btn-block">Kembali</a>
                    <button type="submit" class="btn btn-info btn-block">Simpan</button>
            	</form>
            </div>
		</div>
	</div>
</div>