<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-8">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
         		<form method="post" action="<?php echo site_url()?>penilai/simpan">
                	<div class="form-group">
                      	<label>Penilai <span class="text-danger">*</span></label>
                      	<select class="form-control input-sm" name="choose_evaluator[]" multiple>
                        <?php
                            $evaluator = $this->db->select('a.first_name,a.id')->join('users_groups as b', 'a.id = b.user_id', 'left')->where('b.group_id', 3)->get('users as a');
                            if($evaluator->num_rows() > 0){
                                foreach ($evaluator->result_array() as $k => $v) {
                                    echo '<option value="'.$v['id'].'">'.$v['first_name'].'</option>';
                                }
                            }
                        ?>
                      	</select>
                   </div>
                   <div class="form-group">
                        <label>Test <span class="text-danger">*</span></label>
                        <select class="form-control input-sm" name="test_type" />
                        <?php
                        $test_type = $this->db->select('id, name')->where('status', 1)->where('noedited IS NULL')->get('test_type');
                        if($test_type->num_rows() > 0){
                            foreach ($test_type->result_array() as $key => $value) {
                                echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                            }
                        }
                        ?>
                        </select>
                   </div>
                   <a href="<?php echo site_url('penilai') ?>" class="btn btn-default btn-block">Kembali</a>
                   <button type="submit" class="btn btn-info btn-block">Simpan</button>
         		</form>
      		</div>
   		</div>
	</div>
</div>

<script type="text/javascript">
    

    $(function(){

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>