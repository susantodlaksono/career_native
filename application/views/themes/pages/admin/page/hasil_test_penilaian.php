<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.css">
<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="javascript:history.back()" class="btn btn-default btn-sm">
                Kembali
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <form method="post" action="<?php echo site_url()?>hasil_test/beri_penilaian">
                <input type="hidden" name="q_type_id" value="<?php echo $q_type_id ?>">
                <input type="hidden" name="trans_id" value="<?php echo $trans_id ?>">
                <input type="hidden" name="test_id" value="<?php echo $test_id ?>">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <input type="hidden" name="vacancyid" value="<?php echo $vacancyid ?>">
                <table id="dt-table">
                    <thead>
                        <th>Pertanyaan</th>
                        <th>File</th>
                        <th>Gambar</th>
                        <th>Jawaban</th>
                        <th>Nilai</th>
                    </thead>
                    <tbody>
                        <?php
                        if($pertanyaan){
                            foreach ($pertanyaan as $k => $v) {
                                echo '<tr>';
                                   if($v['question_text']){
                                    echo '<td>'.strip_tags($v['question_text']).'</td>';
                                }else{
                                    echo '<td class="text-danger">'.$v['id'].'</td>';   
                                }
                                if($v['file']){
                                    echo '<td>';
                                    echo '<a href="'.base_url().'files/pertanyaan_file/'.$v['file'].'">'.$v['temp_name_file'].'</a>';
                                    echo '</td>';
                                }else{
                                    echo '<td class="text-danger">None</td>';    
                                }
                                if($v['image']){
                                    echo '<td>';
                                    echo '<a data-fancybox="gallery" href="'.base_url().'files/pertanyaan_gambar/'.$v['image'].'">';
                                        echo '<img src="'.base_url().'files/pertanyaan_gambar/'.$v['image'].'" width="50" height="50">';
                                    echo '</a>';
                                    echo '</td>';
                                }else{
                                    echo '<td class="text-danger">None</td>';
                                }
                                if($v['jawaban_pelamar']){
                                    if($v['jawaban_pelamar']['answers']){
                                        if($q_type_id == 2){
                                            $ext = pathinfo('./files/pelamar_upload/'.$v['jawaban_pelamar']['answers'].'', PATHINFO_EXTENSION);
                                            echo '<td>';
                                            if(in_array($ext, array('png', 'jpg', 'jpeg', 'gif'))){
                                                echo '<a data-fancybox="gallery" href="'.base_url().'files/pelamar_upload/'.$v['jawaban_pelamar']['answers'].'">';
                                                    echo '<img src="'.base_url().'files/pelamar_upload/'.$v['jawaban_pelamar']['answers'].'" width="50" height="50">';
                                                echo '</a>';
                                            }else{
                                                if($ext == 'pdf'){
                                                    echo '<a target="_blank" href="'.base_url().'files/pelamar_upload/'.$v['jawaban_pelamar']['answers'].'">'.$v['jawaban_pelamar']['answers_temp_file'].'</a>';
                                                }else{
                                                    echo '<a href="'.base_url().'files/pelamar_upload/'.$v['jawaban_pelamar']['answers'].'">'.$v['jawaban_pelamar']['answers_temp_file'].'</a>';
                                                }
                                            }
                                        }
                                        if($q_type_id == 3){
                                            echo '<input type="hidden" id="text-answers-'.$v['jawaban_pelamar']['id'].'" value="'.$v['jawaban_pelamar']['answers'].'">';
                                            echo '<td><a class="btn btn-default btn-xs btn-detail" data-id="'.$v['jawaban_pelamar']['id'].'">Lihat Detail Jawaban</a></td>';
                                        }
                                        echo '</td>';
                                        echo '<td width="10"><input name="nilai['.$v['jawaban_pelamar']['id'].']" placeholder="bobot 1 sd 10" value="'.$v['jawaban_pelamar']['result'].'"></td>';
                                    }else{
                                        echo '<td class="text-danger">None</td>';    
                                    }
                                }else{
                                    echo '<td class="text-danger">None</td>';
                                }
                                echo '</tr>';
                            }
                        }else{
                            echo '<tr><td colspan="10">Tidak ditemukan</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
                <?php if($pertanyaan){ ?>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-info text-center">Simpan Penilaian</button>
                    </div>
                </div>
                <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail-desc" tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                    <h5 class="bold">Detail Jawaban</h5> 
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        $(this).on('click', '.btn-detail', function (e){
            e.preventDefault();
            var id = $(this).data('id');
            var desc = $('#text-answers-'+id+'').val();
            $('#modal-detail-desc').find('.modal-body').html(desc);
            $('#modal-detail-desc').modal('show');
        });
    });

</script>