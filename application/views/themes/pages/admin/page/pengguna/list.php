<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="<?php echo site_url() ?>pengguna/tambah" class="btn btn-default btn-sm">
                <i class="fa fa-plus"></i> Tambah
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($pengguna as $k => $v) {
                            echo '<tr>';
                            echo '<td>'.$v['first_name'].'</td>';
                            echo '<td>'.$v['username'].'</td>';
                            echo '<td>'.$v['email'].'</td>';
                            echo '<td>'.$v['role_group'].'</td>';
                            echo '<td>'.($v['active'] == 1 ? 'Active' : 'Inactive').'</td>';
                            echo '<td>';
                                echo '<div class="btn-group">';
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'pengguna/edit/'.$v['id'].'">Edit</a>'; 
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'pengguna/hapus/'.$v['id'].'">Delete</a>';
                                echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable({
            "order": []
        });

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>