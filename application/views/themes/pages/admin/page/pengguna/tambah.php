<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-6">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
         		<form method="post" action="<?php echo site_url()?>pengguna/simpan">
                	<div class="form-group">
                      	<label>Nama <span class="text-danger">*</span></label>
                        <input type="text" name="first_name" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label>Username <span class="text-danger">*</span></label>
                        <input type="text" name="username" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label>Email <span class="text-danger">*</span></label>
                        <input type="email" name="email" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label>Password <span class="text-danger">*</span></label>
                        <input type="password" name="password" class="form-control input-sm">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="active">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role[]">
                            <?php 
                            foreach ($role as $v) {
                                echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <a href="<?php echo site_url('pengguna') ?>" class="btn btn-default btn-block">Kembali</a>
                    <button type="submit" class="btn btn-info btn-block">Simpan</button>
         		</form>
      		</div>
   		</div>
	</div>
</div>

<script type="text/javascript">
    

    $(function(){

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>